-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2018 at 11:51 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_yeni`
--

-- --------------------------------------------------------

--
-- Table structure for table `kuisioner`
--

CREATE TABLE `kuisioner` (
  `id_kuis` int(11) NOT NULL,
  `id_dimensi` int(11) NOT NULL,
  `soal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuisioner`
--

INSERT INTO `kuisioner` (`id_kuis`, `id_dimensi`, `soal`) VALUES
(1, 1, 'Tersedianya pelayanan rawat jalan spesialistik yang minimal harus ada di RSUD Basemah?(*standar RS terdapat poli anak, penyakit dalam, kebidanan, bedah, mata & gigi).'),
(2, 1, 'Ketersediaan informasi RSUD Basemah seperti jadwal dokter, jam buka pelayanan, dll?'),
(3, 1, 'Terdapat tenaga medis yang kompeten di klinik umum maupun di klinik spesialis?'),
(4, 1, 'Tersedianya alat yang lengkap dan modern?'),
(5, 2, 'Dokter yang bertugas menjelaskan penyakit yang diderita pasien dengan baik/jelas?'),
(6, 2, 'Tergambarnya tanggung jawab dokter untuk memberikan informasi kepada pasien dan mendapatkan persetujuan pasien akan tindakan medik yang akan dilakukan?'),
(7, 2, 'Kehadiran dokter untuk memeriksa tepat waktu (sesuai jadwal)?'),
(8, 3, 'Dokter yang bertugas mendengarkan keluhan pasien dengan seksama?'),
(9, 3, 'Bagian pendaftaran melayani dengan baik dan cepat?'),
(10, 3, 'Ketanggapan dokter yang bertugas selama melayani pasien tepat dan cepat?'),
(11, 4, 'Dokter mempunyai kemampuan dalam menetapkan diagnosa penyakit dan mengobati dengan baik, sehingga mampu menimbulkan rasa keyakinan untuk sembuh?'),
(12, 4, 'Dokter yang bertugas memberikan kesempatan kepada pasien dan keluarga untuk bertanya?'),
(13, 4, 'Waktu penyediaan dokumen rekam medik untuk pelayanan rawat jalan ?10 menit?'),
(14, 4, 'Waktu pelayanan yang diterima bagi pasien rawat jalan ? 60 menit?'),
(15, 5, 'Perhatian dokter yang bertugas untuk bertanya keadaan pasien/perkembangannya?'),
(16, 5, 'Perhatian perawat untuk melihat kondisi pasien dan menanyakan keadaan pasien?'),
(17, 5, 'Dokter yang bertugas selalu bersikap ramah dan sopan dalam melayani pasien?');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kuisioner`
--
ALTER TABLE `kuisioner`
  ADD PRIMARY KEY (`id_kuis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kuisioner`
--
ALTER TABLE `kuisioner`
  MODIFY `id_kuis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
