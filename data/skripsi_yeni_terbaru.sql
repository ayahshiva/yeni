-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2018 at 10:19 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_yeni`
--

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` int(11) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `nip`, `nama`, `status`) VALUES
(1, '123456788', 'Dr. Harun', 1),
(2, '13123123123', 'Dr. Hidayat', 1),
(3, '12314123', 'Dr. Shiva', 1),
(4, '513123', 'Dr. Sheva', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `id_poli` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `hari` varchar(20) NOT NULL,
  `jam` time DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_poli`, `id_dokter`, `hari`, `jam`, `status`) VALUES
(1, 7, 2, 'Senin', '10:00:00', 1),
(2, 1, 1, 'Selasa', '08:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jawab_kuis`
--

CREATE TABLE `jawab_kuis` (
  `id_jawab` int(11) NOT NULL,
  `id_kuis` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `p1` int(11) NOT NULL,
  `e1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawab_kuis`
--

INSERT INTO `jawab_kuis` (`id_jawab`, `id_kuis`, `id_pasien`, `p1`, `e1`) VALUES
(1, 1, 3, 1, 1),
(2, 2, 3, 1, 1),
(3, 3, 3, 1, 1),
(4, 4, 3, 1, 1),
(5, 5, 3, 1, 1),
(6, 6, 3, 1, 1),
(7, 7, 3, 1, 1),
(8, 8, 3, 1, 1),
(9, 9, 3, 1, 1),
(10, 10, 3, 1, 1),
(11, 11, 3, 1, 1),
(12, 12, 3, 1, 1),
(13, 13, 3, 1, 1),
(14, 14, 3, 1, 1),
(15, 15, 3, 1, 1),
(16, 16, 3, 1, 1),
(17, 17, 3, 1, 1),
(18, 1, 2, 2, 2),
(19, 2, 2, 2, 2),
(20, 3, 2, 2, 2),
(21, 4, 2, 2, 2),
(22, 5, 2, 2, 2),
(23, 6, 2, 2, 2),
(24, 7, 2, 2, 2),
(25, 8, 2, 2, 2),
(26, 9, 2, 2, 2),
(27, 10, 2, 2, 2),
(28, 11, 2, 2, 2),
(29, 12, 2, 2, 2),
(30, 13, 2, 2, 2),
(31, 14, 2, 2, 2),
(32, 15, 2, 2, 2),
(33, 16, 2, 2, 2),
(34, 17, 2, 2, 2),
(35, 1, 1, 1, 3),
(36, 2, 1, 1, 3),
(37, 3, 1, 1, 3),
(38, 4, 1, 1, 3),
(39, 5, 1, 1, 3),
(40, 6, 1, 1, 3),
(41, 7, 1, 1, 3),
(42, 8, 1, 1, 3),
(43, 9, 1, 1, 3),
(44, 10, 1, 1, 3),
(45, 11, 1, 1, 3),
(46, 12, 1, 1, 3),
(47, 13, 1, 1, 3),
(48, 14, 1, 1, 3),
(49, 15, 1, 1, 3),
(50, 16, 1, 1, 3),
(51, 17, 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `kuisioner`
--

CREATE TABLE `kuisioner` (
  `id_kuis` int(11) NOT NULL,
  `soal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuisioner`
--

INSERT INTO `kuisioner` (`id_kuis`, `soal`) VALUES
(1, 'Tersedianya pelayanan rawat jalan spesialistik yang minimal harus ada di RSUD Basemah?(*standar RS terdapat poli anak, penyakit dalam, kebidanan, bedah, mata & gigi).'),
(2, 'Ketersediaan informasi RSUD Basemah seperti jadwal dokter, jam buka pelayanan, dll?'),
(3, 'Terdapat tenaga medis yang kompeten di klinik umum maupun di klinik spesialis?'),
(4, 'Tersedianya alat yang lengkap dan modern?'),
(5, 'Dokter yang bertugas menjelaskan penyakit yang diderita pasien dengan baik/jelas?'),
(6, 'Tergambarnya tanggung jawab dokter untuk memberikan informasi kepada pasien dan mendapatkan persetujuan pasien akan tindakan medik yang akan dilakukan?'),
(7, 'Kehadiran dokter untuk memeriksa tepat waktu (sesuai jadwal)?'),
(8, 'Dokter yang bertugas mendengarkan keluhan pasien dengan seksama?'),
(9, 'Bagian pendaftaran melayani dengan baik dan cepat?'),
(10, 'Ketanggapan dokter yang bertugas selama melayani pasien tepat dan cepat?'),
(11, 'Dokter mempunyai kemampuan dalam menetapkan diagnosa penyakit dan mengobati dengan baik, sehingga mampu menimbulkan rasa keyakinan untuk sembuh?'),
(12, 'Dokter yang bertugas memberikan kesempatan kepada pasien dan keluarga untuk bertanya?'),
(13, 'Waktu penyediaan dokumen rekam medik untuk pelayanan rawat jalan ?10 menit?'),
(14, 'Waktu pelayanan yang diterima bagi pasien rawat jalan ? 60 menit?'),
(15, 'Perhatian dokter yang bertugas untuk bertanya keadaan pasien/perkembangannya?'),
(16, 'Perhatian perawat untuk melihat kondisi pasien dan menanyakan keadaan pasien?'),
(17, 'Dokter yang bertugas selalu bersikap ramah dan sopan dalam melayani pasien?');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id_login` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_login`, `username`, `password`, `email`, `role`) VALUES
(1, 'admin', '$2a$08$1w4FY0b9wt.oixK6UZYiCeYm85hwtxYsA.noJiGjDjawHn2f/cMuK', 'admin@website.com', 1),
(4, 'kelik', '$2a$08$Ver6X..Yff.PfkzMq0BElu0aDdRHUNWAJ2SbF8FLAk9xk0ny4.jKW', 'kelik@gmail.com', 2),
(5, 'shiva', '$2a$08$MUBfoUggh6CsXqnRAo.MJuOBnZyuj0mXfSaCGSKDWhUo8G/zb/ssq', 'shiva@gmail.com', 2),
(6, 'sheva', '$2a$08$ldf6qenulGRRWcbb8ZY.xu1yT0Zu.7sfTqlbYojarJHMVABtjkYYK', 'sheva@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` int(11) NOT NULL,
  `no_rm` varchar(100) NOT NULL,
  `id_login` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `no_ktp` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `telp` varchar(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `jaminan` varchar(50) NOT NULL,
  `survey` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `no_rm`, `id_login`, `tanggal`, `no_ktp`, `nama`, `gender`, `tgl_lahir`, `telp`, `alamat`, `pekerjaan`, `jaminan`, `survey`) VALUES
(1, 'RM181227043023', 4, '2018-12-27', '1212334312', 'kelik', 'Laki-Laki', '1982-04-17', '082281127512', 'lemabang', 'pedagang', 'Umum', 2),
(2, 'RM181227043217', 5, '2018-12-27', '', '', '', '0000-00-00', '', '', '', '', 2),
(3, 'RM181228040912', 6, '2018-12-28', '', '', '', '0000-00-00', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id_adu` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `komentar` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `poli`
--

CREATE TABLE `poli` (
  `id_poli` int(11) NOT NULL,
  `kode_poli` varchar(100) NOT NULL,
  `nama_poli` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poli`
--

INSERT INTO `poli` (`id_poli`, `kode_poli`, `nama_poli`, `status`) VALUES
(1, 'PLKL', 'Poli Kulit', 1),
(2, 'PLPD', 'Poli Penyakit Dalam', 1),
(3, 'PLKD', 'Poli Kebidanan', 1),
(4, 'PLAK', 'Poli Anak', 1),
(5, 'PLBD', 'Poli Bedah', 1),
(6, 'PLMT', 'Poli Mata', 1),
(7, 'PMCU', 'Poli MCU', 1),
(8, 'PLGM', 'Poli Gigi dan Mulut', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rekam_medis`
--

CREATE TABLE `rekam_medis` (
  `id_rm` int(11) NOT NULL,
  `no_rm` varchar(100) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `id_poli` int(11) NOT NULL,
  `jmn` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekam_medis`
--

INSERT INTO `rekam_medis` (`id_rm`, `no_rm`, `id_pasien`, `tgl_daftar`, `id_poli`, `jmn`, `status`) VALUES
(3, 'RM181227043023', 1, '2018-12-28', 8, 'BPJS', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `jawab_kuis`
--
ALTER TABLE `jawab_kuis`
  ADD PRIMARY KEY (`id_jawab`);

--
-- Indexes for table `kuisioner`
--
ALTER TABLE `kuisioner`
  ADD PRIMARY KEY (`id_kuis`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`),
  ADD UNIQUE KEY `no_rm` (`no_rm`);

--
-- Indexes for table `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id_adu`);

--
-- Indexes for table `poli`
--
ALTER TABLE `poli`
  ADD PRIMARY KEY (`id_poli`),
  ADD UNIQUE KEY `kode_poli` (`kode_poli`);

--
-- Indexes for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD PRIMARY KEY (`id_rm`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id_dokter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jawab_kuis`
--
ALTER TABLE `jawab_kuis`
  MODIFY `id_jawab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `kuisioner`
--
ALTER TABLE `kuisioner`
  MODIFY `id_kuis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengaduan`
--
ALTER TABLE `pengaduan`
  MODIFY `id_adu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `poli`
--
ALTER TABLE `poli`
  MODIFY `id_poli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  MODIFY `id_rm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
