    <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="#">Dashboard</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Form Profil</li>
                                    </ul>
                                </div>
                               <!--  <form class="au-form-icon--sm" action="" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
                                    <button class="au-btn--submit2" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Halo 
                                <span><?php echo $this->session->userdata('username'); ?>!</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Form Profil Pasien</h3>
                            <div class="col-lg-10 offset-lg-1">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Profil Pasien</strong>
                                        <small> Form</small>
                                    </div>
                                    <div class="card-body card-block" id="printableArea">

                                    <?php if($this->session->flashdata('sukses') !=''){ ?>
                                        <div class="sufee-alert alert with-close alert-primary alert-dismissible fade show">
                                            <span class="badge badge-pill badge-primary">Success</span>
                                            <?php echo $this->session->flashdata('sukses'); ?>.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php } ?>
                                    <form method="post" action="<?php echo base_url('user/update_profil'); ?>">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">NIK</label>
                                            <input type="hidden" name="id_login" value="<?php echo $this->session->userdata('id_login'); ?>" class="form-control">
                                            <input type="text" id="ktp" placeholder="No KTP" name="no_ktp" value="<?php echo $user->no_ktp; ?>" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="vat" class=" form-control-label">Nama</label>
                                            <input type="text" id="nama" placeholder="Nama" name="nama" value="<?php echo $user->nama; ?>" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="street" class=" form-control-label">Gender</label>
                                            <select name="gender" id="select" class="form-control">
                                                <?php if($user->gender ==''){ ?>
                                                    <option value="" selected="selected">Jenis Kelamin</option>
                                                    <option value="Laki-Laki">Laki-Laki</option>
                                                    <option value="Perempuan">Perempuan</option> 
                                                <?php }elseif($user->gender =='Laki-Laki'){ ?>
                                                    <option value="Laki-Laki" selected="selected">Laki-Laki</option>
                                                     <option value="Perempuan">Perempuan</option> 
                                                <?php }else{ ?>
                                                    <option value="Laki-Laki">Laki-Laki</option>
                                                    <option value="Perempuan" selected="selected">Perempuan</option>     
                                                <?php } ?>    
                                            </select>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-8">
                                                <div class="form-group">
                                                    <label for="city" class=" form-control-label">Tanggal Lahir</label>
                                                    <input type="date" id="tgl_lahir placeholder="Tanggal Lahir" class="form-control" name="tgl_lahir" value="<?php echo $user->tgl_lahir; ?>">
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <div class="form-group">
                                                    <label for="postal-code" class=" form-control-label">Telepon</label>
                                                    <input type="text" id="telp" placeholder="Telepon / HP" class="form-control" name="telp" value="<?php echo $user->telp; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="country" class=" form-control-label">Alamat</label>
                                            <textarea name="alamat" id="alamat" rows="3" placeholder="Alamat" class="form-control"><?php echo $user->alamat; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="jaminan" class="form-control-label">Pekerjaan</label>
                                            <input type="text" id="pekerjaan" placeholder="Pekerjaan" class="form-control" name="pekerjaan" value="<?php echo $user->pekerjaan; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="jaminan" class="form-control-label">Jaminan</label>
                                           <!--  <input type="text" id="jaminan" placeholder="Jaminan" class="form-control" name="jaminan" value="<?php echo $user->jaminan; ?>"> -->
                                           <select name="jaminan" id="select" class="form-control">
                                                <option>Pilih Jaminan</option>
                                                <option value="Umum" <?php if($user->jaminan == 'Umum'){echo "selected";} ?> >Umum</option>
                                                <option value="BPJS" <?php if($user->jaminan == 'BPJS'){echo "selected";} ?> >BPJS</option>
                                                <option value="Swasta" <?php if($user->jaminan == 'Swasta'){echo "selected";} ?> >Swasta</option>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <input type="submit" class="btn btn-primary btn-sm" name="submit"> | 

                                        <input type="button" class="btn btn-primary btn-sm" onclick="printDiv('printableArea')" value="Cetak" />
                                    </div>
                                </form>
                                </div>
                            </div>  

                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
        </div>
    <!-- END CONTENT -->

<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
          

           