<!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="#">Dashboard</a>
                                        </li>
                                       <!--  <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Dashboard</li> -->
                                    </ul>
                                </div>
                               <!--  <form class="au-form-icon--sm" action="" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
                                    <button class="au-btn--submit2" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Halo 
                                <span><?php echo $this->session->userdata('username'); ?>!</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Jadwal Kunjungan</h3>
                            
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>No RM</th>
                                            <th>Tanggal</th>
                                            <th>Poli</th>
                                            <th>Jaminan</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($kueri as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value->no_rm; ?></td>
                                            <td><?php echo $value->tgl_daftar; ?></td>
                                            <td><?php echo $value->nama_poli; ?></td>
                                            <td><?php echo $value->jmn; ?></td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->

            <?php if($survey->survey == 1){ ?>

            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Kuisioner</h3>
                            <h5>Berikan penilaian anda untuk masing masing pertanyaan</h5>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">No</th>
                                            <th rowspan="2">Pertanyaan</th>
                                            <th colspan="5" align="center">Persepsi</th>
                                            <th colspan="5" align="center">Ekspektasi</th>
                                        </tr>
                                        <tr>
                                            <th>1</th>
                                            <th>2</th>
                                            <th>3</th>
                                            <th>4</th>
                                            <th>5</th>

                                            <th>1</th>
                                            <th>2</th>
                                            <th>3</th>
                                            <th>4</th>
                                            <th>5</th>
                                        </tr>
                                    </thead>
                                    <form action="<?php echo base_url('user/simpan_survey'); ?>" method="post">
                                        <tbody>
                                            <?php foreach ($soal as $key => $value) { ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $value->id_kuis; ?>
                                                        <input type="hidden" name="id_kuis[]" value="<?php echo $value->id_kuis; ?>">
                                                    </td>
                                                    <td>
                                                        <?php echo $value->soal; ?>
                                                        <input type="hidden" name="id_pasien[]" value="<?php echo $id_pasien; ?>">   
                                                    </td>
                                                    <td><input type="radio" required="required" name="p[<?php echo $value->id_kuis; ?>]" value="1"></td>
                                                    <td><input type="radio" required="required" name="p[<?php echo $value->id_kuis; ?>]" value="2"></td>
                                                    <td><input type="radio" required="required" name="p[<?php echo $value->id_kuis; ?>]" value="3"></td>
                                                    <td><input type="radio" required="required" name="p[<?php echo $value->id_kuis; ?>]" value="4"></td>
                                                    <td><input type="radio" required="required" name="p[<?php echo $value->id_kuis; ?>]" value="5"></td>

                                                    <td><input type="radio" required="required" name="e[<?php echo $value->id_kuis; ?>]" value="1"></td>
                                                    <td><input type="radio" required="required" name="e[<?php echo $value->id_kuis; ?>]" value="2"></td>
                                                    <td><input type="radio" required="required" name="e[<?php echo $value->id_kuis; ?>]" value="3"></td>
                                                    <td><input type="radio" required="required" name="e[<?php echo $value->id_kuis; ?>]" value="4"></td>
                                                    <td><input type="radio" required="required" name="e[<?php echo $value->id_kuis; ?>]" value="5"></td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>
                                                    <input type="submit" class="btn btn-primary btn-sm" name="Kirim">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </form>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>                
            </section>

            <?php } ?>
        </div>
           

