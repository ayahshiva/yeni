    <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="#">Dashboard</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Rawat Jalan</li>
                                    </ul>
                                </div>
                               <!--  <form class="au-form-icon--sm" action="" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
                                    <button class="au-btn--submit2" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Halo 
                                <span><?php echo $this->session->userdata('username'); ?>!</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Form Daftar Rawat Jalan</h3>
                            <div class="col-lg-10 offset-lg-1">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Daftar Rawat Jalan</strong>
                                        <small> Form</small>
                                    </div>
                                    <div class="card-body card-block">

                                    <?php if($this->session->flashdata('sukses') !=''){ ?>
                                        <div class="sufee-alert alert with-close alert-primary alert-dismissible fade show">
                                            <span class="badge badge-pill badge-primary">Success</span>
                                            <?php echo $this->session->flashdata('sukses'); ?>.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php } ?>
                                    <form method="post" action="<?php echo base_url('user/simpan_rekam_medis'); ?>">
                                        <div class="form-group">
                                            <label for="company" class=" form-control-label">No Rekam Medis</label>
                                            <input type="hidden" name="id_pasien" value="<?php echo $user->id_pasien; ?>" class="form-control">
                                            <input type="text" id="no_rm" placeholder="No Rekam Medis" name="no_rm" readonly="readonly" class="form-control" value="<?php echo $user->no_rm; ?>">
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-8">
                                                <div class="form-group">
                                                    <label for="vat" class=" form-control-label">Tanggal</label>
                                                    <input type="date" id="tgl_daftar" placeholder="Input Tanggal Daftar" name="tgl_daftar" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <div class="form-group">
                                                    <label for="poli" class="form-control-label">Pilih Poli</label>
                                                    <select name="id_poli" id="select" class="form-control">
                                                        <option>Pilih Poli</option>
                                                        <?php foreach($poli as $list){ ?>
                                                            <option value="<?php echo $list->id_poli; ?>"><?php echo $list->nama_poli; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="jaminan" class="form-control-label">Jaminan</label>
                                            <!-- <input type="text" id="jaminan" placeholder="Jaminan" class="form-control" name="jaminan"> -->
                                            <select name="jaminan" id="select" class="form-control">
                                                <option>Pilih Jaminan</option>
                                                <option value="Umum">Umum</option>
                                                <option value="BPJS">BPJS</option>
                                                <option value="Swasta">Swasta</option>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <input type="submit" class="btn btn-primary btn-sm" name="submit">
                                    </div>
                                </form>
                                </div>
                            </div>  

                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
        </div>
    <!-- END CONTENT->
          

           