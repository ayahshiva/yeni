    <section class="wthree-row pt-3 pb-lg-5 w3-contact">
        <div class="container py-sm-5 pt-0 pb-5">
            <div class="title-section text-center pb-lg-5">
                <h3 class="w3ls-title text-center text-capitalize">form pengaduan</h3>
            </div>
            <div class="row contact-form pt-lg-5">
                <!-- contact details -->
                <div class="col-lg-4 contact-bottom mt-lg-0 mt-5">
                    <div class="contact-details-top">
                        <h5 class="sub-title-wthree">Hubungi Kami</h5>
                        <div class="row wthree-cicon">
                            <span class="fas fa-envelope-open mr-3"></span>
                            <a href="mailto:info@example.com">info@rsud--basemah.com</a>
                        </div>
                        <div class="row wthree-cicon">
                            <span class="fas fa-phone-volume mr-3"></span>
                            <h6>+62711 123 7890</h6>
                        </div>
                        <div class="row wthree-cicon">
                            <span class="fas fa-globe mr-3"></span>
                            <a href="#">www.rsud-basemah.com</a>
                        </div>
                    </div>
                    <div class="address">
                                              
                    </div>
                    <div class="footerv2-w3ls">
                        <h5 class="sub-title-wthree">follow us</h5>
                        <ul class="social-iconsv2 agileinfo">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- //contact details -->
                <div class="col-lg-8 wthree-form-left px-lg-5 mt-lg-0 mt-5">
                    <!-- contact form grid -->
                    <div class="contact-top1">
                        <h5 class="sub-title-wthree">form pengaduan</h5>
                        <form action="<?php echo base_url('home/simpan_pengaduan'); ?>" method="post" class="pc-contact">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="contact-name">Nama
                                        <span>*</span>
                                    </label>
                                    <input type="text" class="form-control" id="contact-name" name="nama" required>
                                    <input type="hidden" name="tgl" value="<?php echo date('Y-m-d'); ?>">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="contact-email">Email
                                        <span>*</span>
                                    </label>
                                    <input type="email" class="form-control" id="contact-email" name="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact-subject">Judul
                                    <span>*</span>
                                </label>
                                <input type="text" class="form-control" id="contact-subject" name="judul" required>
                            </div>
                            <div class="form-group">
                                <label for="contact-message">
                                    Isi Pesan
                                    <span>*</span>
                                </label>
                                <textarea class="form-control" rows="5" id="contact-message" name="isi" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block w-25">Send</button>
                        </form>
                    </div>
                    <!--  //contact form grid ends here -->
                </div>
            </div>
            <!-- //contact details container -->
        </div>
        <!-- contact map grid -->
        <div class="map contact-right p-sm-5 p-3 pb-lg-5">
            <div class="title-section text-center pb-5">
                <h4>RSUD Basemah</h4>
                <h3 class="w3ls-title text-center text-capitalize">Peta Lokasi</h3>
            </div>

           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.4055486099883!2d104.73047331410595!3d-2.984832997826169!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e3b75ed85ff995b%3A0xe6b7ce232ced4e94!2sSriwijaya+University!5e0!3m2!1sen!2ssg!4v1546426002846" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!--//contact map grid ends here-->
    </section>