
    <!-- breadcrumbs -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Beranda</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Profil</li>
        </ol>
    </nav>
    <!-- //breadcrumbs -->
    <!-- //banner -->
    <div class="section-2 py-md-5 py-3">
        <div class="container">
            <div class="title-section text-center pb-5">
                <h4>RSUD Basemah</h4>
                <h3 class="w3ls-title text-center text-capitalize">Rumah Sakit yang Terpercaya</h3>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row slide">
                <div class="col-lg-4 triple-sec">
                    <h5 class="text-dark">Keungulan</h5>
                    <ul class="list-group mt-3">
                        <li class="list-group-item border-0">
                            <i class="fas fa-heartbeat mr-3"></i>Cras justo odio</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-user-md mr-3"></i>Dapibus ac facilisis in</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-pills mr-3"></i>Morbi leo risus</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-thermometer mr-3"></i>
                            Porta ac consectetur ac</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-ambulance mr-3"></i>Vestibulum at eros</li>
                    </ul>
                </div>
                <div class="col-lg-4  triple-sec">
                    <h5>Jam Buka Layanan</h5>
                    <ul class="list-unstyled">
                        <li class="clearfix py-3">
                            <span class="float-left"> Senin - Kamis </span>
                            <div class="value float-right"> 08.00 - 13.00 </div>
                        </li>
                        <li class="clearfix border-top border-bottom my-3 py-3">
                            <span class="float-left"> Jum'at </span>
                            <div class="value float-right"> 08.00 - 11.00 </div>
                        </li>
                        <li class="clearfix py-3">
                            <span class="float-left"> Minggu </span>
                            <div class="value float-right"> 08.00 - 12.30 </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 triple-sec">
                    <h5 class="text-black">Profil Singkat</h5>
                   <p class="pt-4">Rumah Sakit Umum Daerah Besemah Kota Pagar Alam adalah milik Pemerintah Kota Pagar Alam yang berdiri pada tahun 1954 dan diresmikan oleh Wakil Presiden RI <strong> DR. Mohammad Hatta </strong> pada tahun 1958 dengan nama Rumah Sakit Umum Pagar Alam.</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <section class="agile_stats">
        <div class="container py-5">
            <div class="stats_agile my-lg-5 mb-5">
                <h3 class="stat-title text-capitalize text-white">Syarat dan Ketentuan</h3>
                <hr>
                <p class="mt-3 text-white">
                    - Untuk melakukan pendaftaran awal diharapkan pasien melengkapi biodata diri terlebih dahulu, kemudian cetak kartu registrasi yang tertera di website masing-masing. <br />
                    -Bagi pasien BPJS untuk melakukan rawat jalan harap membawa fotokopy ktp & surat rujukan.
                </p>
            </div>
            <div class="row pb-lg-5 w3-abbottom">
                
            </div>
        </div>
    </section>
    