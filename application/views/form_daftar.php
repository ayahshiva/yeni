
    <!-- breadcrumbs -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Beranda</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Daftar</li>
        </ol>
    </nav>
    <!-- //breadcrumbs -->
    <!-- //banner -->
    <!-- contact -->
    <section class="wthree-row pt-3 pb-lg-5 w3-contact">
        <div class="container py-sm-5 pt-0 pb-5">
            <div class="title-section text-center pb-lg-5">
                <!-- <h4>24/7</h4> -->
                <h3 class="w3ls-title text-center text-capitalize">Form Daftar Pasien Baru</h3>
            </div>
            <div class="row contact-form pt-lg-5">
                <!-- contact details -->
                <div class="col-lg-4 contact-bottom mt-lg-0 mt-5">
                    <div class="contact-details-top">
                       <h5 class="sub-title-wthree">Hubungi Kami</h5>
                        <div class="row wthree-cicon">
                            <span class="fas fa-envelope-open mr-3"></span>
                            <a href="mailto:info@example.com">info@rsud-basemah.com</a>
                        </div>
                        <div class="row wthree-cicon">
                            <span class="fas fa-phone-volume mr-3"></span>
                            <h6>0711 717171</h6>
                        </div>
                        <div class="row wthree-cicon">
                            <span class="fas fa-globe mr-3"></span>
                            <a href="#">www.rsud-basemah.com</a>
                        </div>
                    </div>
                    <br />
                    <div class="footerv2-w3ls">
                        <h5 class="sub-title-wthree">sosial media</h5>
                        <ul class="social-iconsv2 agileinfo">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- //contact details -->
                <div class="col-lg-8 wthree-form-left px-lg-5 mt-lg-0 mt-5">
                    <!-- contact form grid -->
                    <div class="contact-top1">
                        <h5 class="sub-title-wthree">Form Daftar</h5>
                        <form action="<?php echo base_url('home/simpan_daftar'); ?>" method="post" class="pc-contact">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="contact-name">Username
                                        <span>*</span>
                                    </label>
                                    <input type="text" class="form-control" id="contact-name" name="username" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="contact-email">Email
                                        <span>*</span>
                                    </label>
                                    <input type="email" class="form-control" id="contact-email" name="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact-subject">Password
                                    <span>*</span>
                                </label>
                                <input type="password" class="form-control" id="contact-subject" name="password" required>
                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-block w-25">Daftar</button>
                        </form>
                    </div>
                    <!--  //contact form grid ends here -->
                </div>
            </div>
            <!-- //contact details container -->
        </div>
    </section>
    