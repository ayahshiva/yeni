
    <!-- section-2 -->
    <div class="section-2">
        <div class="container-fluid">
            <div class="row slide">
                <div class="col-lg-4 triple-sec">
                    <h5 class="text-dark">Keunggulan</h5>
                    <ul class="list-group mt-3">
                        <li class="list-group-item border-0">
                            <i class="fas fa-heartbeat mr-3"></i>Cepat Tanggap</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-user-md mr-3"></i>Tenaga medis sigap</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-pills mr-3"></i>Mutu obat terjamin</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-thermometer mr-3"></i>Perawatan maksimal</li>
                        <li class="list-group-item border-0">
                            <i class="fas fa-ambulance mr-3"></i>Jasa ambulance gratis</li>
                    </ul>
                </div>
                <div class="col-lg-4  triple-sec">
                    <h5>Jam Buka Layanan</h5>
                    <ul class="list-unstyled">
                        <li class="clearfix py-3">
                            <span class="float-left"> Senin - Kamis </span>
                            <div class="value float-right"> 08.00 - 13.00 </div>
                        </li>
                        <li class="clearfix border-top border-bottom my-3 py-3">
                            <span class="float-left"> Jum'at </span>
                            <div class="value float-right"> 08.00 - 11.00 </div>
                        </li>
                        <li class="clearfix py-3">
                            <span class="float-left"> Minggu </span>
                            <div class="value float-right"> 08.00 - 12.30 </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 triple-sec">
                    <h5 class="text-black">Profil Singkat</h5>
                    <p class="pt-4">Rumah Sakit Umum Daerah Besemah Kota Pagar Alam adalah milik Pemerintah Kota Pagar Alam yang berdiri pada tahun 1954 dan diresmikan oleh Wakil Presiden RI <strong> DR. Mohammad Hatta </strong> pada tahun 1958 dengan nama Rumah Sakit Umum Pagar Alam.</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- //section-2 -->
    <!-- about -->
    <div class="agileits-about py-md-5 py-5" id="services">
        <div class="container py-lg-5">
            <div class="title-section text-center pb-md-5">
                <h4>Poly clinic</h4>
                <h3 class="w3ls-title text-center text-capitalize">hospital that you can trust</h3>
            </div>
            <div class="agileits-about-row row  text-center pt-md-0 pt-5">
                <div class="col-lg-4 col-sm-6 agileits-about-grids">
                    <div class="p-md-5 p-sm-3">
                        <i class="fas fa-user-md"></i>
                        <h4 class="mt-2 mb-3">therapist</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur aut</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 agileits-about-grids  border-left border-right my-sm-0 my-5">
                   <div class="p-md-5 p-sm-3">
                        <i class="fas fa-thermometer"></i>
                        <h4 class="mt-2 mb-3">laboratory</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur aut</p>
                    </div>
                </div>
                <div class="col-lg-4 agileits-about-grids">
                   <div class="p-md-5 p-sm-3">
                        <i class="far fa-hospital"></i>
                        <h4 class="mt-2 mb-3">surgery</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur aut</p>
                    </div>
                </div>
            </div>
            <div class="agileits-about-row border-top row text-center pb-lg-5 pt-md-0 pt-5 mt-md-0 mt-5">
                <div class="col-lg-4 col-sm-6 agileits-about-grids">
                    <div class="p-md-5 p-sm-3 col-label">
                        <i class="fas fa-hospital-symbol"></i>
                        <h4 class="mt-2 mb-3">transplants</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur aut</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 agileits-about-grids mt-lg-0 mt-md-3 border-left border-right pt-sm-0 pt-5">
                    <div class="p-md-5 p-sm-3 col-label">
                        <i class="fas fa-ambulance">
                        </i>
                        <h4 class="mt-2 mb-3">emergency care</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur aut</p>
                    </div>
                </div>
                <div class="col-lg-4 agileits-about-grids pt-md-0 pt-5">
                    <div class="p-md-5 p-sm-3 col-label">
                        <i class="fa fa-user-md"></i>
                        <h4 class="mt-2 mb-3">oncology</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores alias consequatur aut</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //about -->
    <!-- blog -->
    <section class="blog_w3ls py-lg-5">
        <div class="container">
            <div class="title-section text-center pb-lg-5">
                <h4>Dunia Kesehatan</h4>
                <h3 class="w3ls-title text-center text-capitalize">Berita Terbaru</h3>
            </div>
            <div class="row py-5">
                <!-- blog grid -->
                <div class="col-lg-4 col-md-6">
                    <div class="card border-0 med-blog">
                        <div class="card-header p-0">
                            <a href="#">
                                <img class="card-img-bottom" src="<?php echo base_url('asset/frontend/'); ?>images/g5.jpg" alt="Card image cap">
                            </a>
                        </div>
                        <div class="card-body border-0 px-0">
                            <div class="blog_w3icon">
                                <span>
                                    May 19,2018 - loremipsum</span>
                            </div>
                            <div class="pt-2">
                                <h5 class="blog-title card-title font-weight-bold">
                                    <a href="single.html">Cras ultricies ligula sed magna dictum porta auris blandita.</a>
                                </h5>
                            </div>
                            <a href="#" class="blog-btn">Read more</a>
                        </div>
                    </div>
                </div>
                <!-- //blog grid -->
                <!-- blog grid -->
                <div class="col-lg-4 col-md-6 mt-md-0 mt-5">
                    <div class="card border-0 med-blog">
                        <div class="card-header p-0">
                            <a href="#">
                                <img class="card-img-bottom" src="<?php echo base_url('asset/frontend/'); ?>images/g2.jpg" alt="Card image cap">
                            </a>
                        </div>
                        <div class="card-body border-0 px-0">
                            <div class="blog_w3icon">
                                <span>
                                    June 21,2018 - loremipsum</span>
                            </div>
                            <div class="pt-2">
                                <h5 class="blog-title card-title font-weight-bold">
                                    <a href="single.html">Cras ultricies ligula sed magna dictum porta auris blandita.</a>
                                </h5>
                            </div>
                            <a href="#" class="blog-btn">Read more</a>
                        </div>
                    </div>
                </div>
                <!-- //blog grid -->
                <!-- blog grid -->
                <div class="col-lg-4 col-md-6 mt-lg-0 mt-5">
                    <div class="card border-0 med-blog">
                        <div class="card-header p-0">
                            <a href="#">
                                <img class="card-img-bottom" src="<?php echo base_url('asset/frontend/'); ?>images/g1.jpg" alt="Card image cap">
                            </a>
                        </div>
                        <div class="card-body border-0 px-0">
                            <div class="blog_w3icon">
                                <span>
                                    July 23,2018 - loremipsum</span>
                            </div>
                            <div class="pt-2">
                                <h5 class="blog-title card-title font-weight-bold">
                                    <a href="single.html">Cras ultricies ligula sed magna dictum porta auris blandita.</a>
                                </h5>
                            </div>
                            <a href="#" class="blog-btn">Read more</a>
                        </div>
                    </div>
                </div>
                <!-- //blog grid -->
            </div>
        </div>
    </section>
    <!-- //blog -->
    