
    <!-- breadcrumbs -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Beranda</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Poliklinik</li>
        </ol>
    </nav>
    <!-- //breadcrumbs -->
    <!-- //banner -->
    <!-- contact -->
    <div class="agileits-services py-lg-5 pb-5" id="explore">
        <div class="container py-lg-5">
            <div class="title-section text-center pb-5 border-bottom">
                <h4>PoliKlinik</h4>
                <h3 class="w3ls-title text-center text-capitalize">RSUD Basemah</h3>
            </div>
            <div class="agileits-services-row row pt-5">
                <div class="col-lg-3 col-sm-6 agileits-services-grids py-lg-5 py-sm-4 pb-4">
                    <span class="fas fa-h-square"></span>
                    <h4 class="my-3">Poli Kulit</h4>
                </div>
                <div class="col-lg-3 col-sm-6 agileits-services-grids  py-lg-5 py-4">
                    <span class="fas fa-heartbeat"></span>
                    <h4 class="my-3">Poli Penyakit Dalam</h4>
                </div>
                <div class="col-lg-3 col-sm-6 agileits-services-grids  py-lg-5 py-4">
                    <span class="fas fa-user-md"></span>
                    <h4 class="my-3">Poli Kebidanan</h4>
                </div>
                <div class="col-lg-3 col-sm-6 agileits-services-grids   py-lg-5 py-4">
                    <span class="fas fa-child"></span>
                    <h4 class="my-3">Poli Anak</h4>
                </div>
            </div>
            <div class="row pb-md-5">
                <div class="col-lg-3 col-sm-6 agile_service_bottom agileits-services-grids  py-lg-5 py-4">
                    <span class="fas fa-hospital"></span>
                    <h4 class="my-3">Poli Bedah</h4>
                </div>
                <div class="col-lg-3 col-sm-6 agile_service_bottom agileits-services-grids  py-lg-5 py-4">
                    <span class="fas fa-eye"></span>
                    <h4 class="my-3">Poli Mata</h4>
                </div>
                <div class="col-lg-3 col-sm-6 agile_service_bottom agileits-services-grids   py-lg-5 py-4">
                    <span class="fas fa-ambulance"></span>
                    <h4 class="my-3">Poli MCU</h4>
                </div>
                <div class="col-lg-3 col-sm-6 agile_service_bottom agileits-services-grids   py-lg-5 py-sm-4 pt-4">
                    <span class="fas fa-stethoscope"></span>
                    <h4 class="my-3">Poli Gigi dan Mulut</h4>
                </div>
            </div>
        </div>
    </div>
    