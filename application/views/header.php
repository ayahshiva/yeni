<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Rumah Sakit Umum Daerah Basemah</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Poly Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="<?php echo base_url('asset/frontend/'); ?>css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('asset/frontend/'); ?>css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="<?php echo base_url('asset/frontend/'); ?>css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>
    <!-- banner -->
    <?php 
        if($this->uri->segment('2') !=''){
            echo "<div class='inner-banner' id='home'>";
        }else{echo "<div class='banner' id='home'>";}
    ?>
    <!-- <div class="banner" id="home"> -->
        <!-- header -->
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-gradient-secondary pt-3">

                <h1>
                    <a class="navbar-brand text-white" href="<?php echo base_url(); ?>">
                        rsud basemah
                    </a>
                </h1>
                <button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-lg-auto text-center">
                        <li class="nav-item  mr-3 active">
                            <a class="nav-link text-white active" href="<?php echo base_url(); ?>">Beranda
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item  mr-3">
                            <a class="nav-link text-white text-capitalize" href="<?php echo base_url('home/profil'); ?>">Profil RSUD</a>
                        </li>
                        <li class="nav-item  mr-3 ">
                            <a class="nav-link text-white text-capitalize" href="<?php echo base_url('home/poliklinik'); ?>">Poliklinik</a>
                        </li>
                         <li class="nav-item  mr-3 ">
                            <a class="nav-link text-white text-capitalize" href="<?php echo base_url('home/jadwal_dokter'); ?>">Jadwal Dokter</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  text-white text-capitalize" href="<?php echo base_url('home/pengaduan'); ?>">Pengaduan</a>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>
        <!-- //header -->
        <?php if($this->uri->segment('2') ==''){ ?>
         <div class="container">
            <!-- banner-text -->
            <div class="banner-text">
                <div class="callbacks_container">
                    <ul class="rslides" id="slider3">
                        <li>
                            <div class="slider-info">
                                <span class="">Pasien Baru</span>
                                <h3>Klik link dibawah untuk mendaftar</h3>
                                <a class="btn btn-primary mt-3" href="<?php echo base_url('home/daftar'); ?>" role="button">daftar</a>
                            </div>
                        </li>
                        <!-- <li>
                            <div class="slider-info">
                                <span class="">providing total</span>
                                <h3>health care solution</h3>
                                <a class="btn btn-primary mt-3" href="services.html" role="button">View Details</a>
                            </div>
                        </li>
                        <li>
                            <div class="slider-info">
                                <span class="">providing total</span>
                                <h3>health care solution</h3>
                                <a class="btn btn-primary mt-3" href="services.html" role="button">View Details</a>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <!-- //container -->
    <?php  } ?>
    </div>
    <!-- //banner -->