    <!-- footer -->
    <footer class="footer py-md-5 pt-md-3 pb-sm-5">
        <div class="footer-position">
            <div class="container">
                <div class="row newsletter-inner">
                    <div class="col-md-4 py-3">
                        <h3 class="w3ls-title text-white">
                            Berlangganan</h3>
                        <p class="text-white">Layanan Berlangganan info terbaru seputar RSUD Basemah.</p>
                    </div>
                    <div class="col-md-8 newsright">
                        <form action="#" method="post" class="d-flex">
                            <input class="form-control" type="email" placeholder="Masukkan Email Anda..." name="email" required="">
                            <input class="form-control" type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="up-arrow"></div>
                </div>
            </div>
        </div>
        <div class="container-fluid py-lg-5 mt-sm-5">
            <div class="row p-sm-4 px-3 py-5">
                <div class="col-lg-4 col-md-6 footer-top mt-lg-0 mt-md-5">
                    <h2>
                        <a href="index.html" class="text-theme text-uppercase">
                            RSUD Basemah
                        </a>
                    </h2>
                    <p class="mt-2">Rumah Sakit Umum Daerah Besemah Kota Pagar Alam adalah milik Pemerintah Kota Pagar Alam yang berdiri pada tahun 1954 dan diresmikan oleh Wakil Presiden RI <strong> DR. Mohammad Hatta </strong> pada tahun 1958 dengan nama Rumah Sakit Umum Pagar Alam.
                    </p>
                </div>
                <div class="col-lg-2  col-md-6 mt-lg-0 mt-5">
                    <div class=".footerv2-w3ls">
                        <h3 class="mb-3 w3f_title">Navigation</h3>
                        <hr>
                        <ul class="list-agileits">
                            <li>
                                <a href="#">
                                    Beranda
                                </a>
                            </li>
                            <li class="my-2">
                                <a href="#">
                                    Profil RSUD
                                </a>
                            </li>
                            <li class="my-2">
                                <a href="#">
                                    Poliklinik
                                </a>
                            </li>
                            <li class="mb-2">
                                <a href="services.html">
                                    Kunjungan
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Pengaduan
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mt-lg-0 mt-5">
                    <div class="footerv2-w3ls">
                        <h3 class="mb-3 w3f_title">Hubungi Kami</h3>
                        <hr>
                        <div class="fv3-contact">
                            <span class="fas fa-envelope-open mr-2"></span>
                            <p>
                                <a href="mailto:example@email.com">info@rsud-basemah.com</a>
                            </p>
                        </div>
                        <div class="fv3-contact my-2">
                            <span class="fas fa-phone-volume mr-2"></span>
                            <p>0711 717171</p>
                        </div>
                        <div class="fv3-contact">
                            <span class="fas fa-home mr-2"></span>
                            <p>Palembang
                                <br>Sumatera Selatan 30116.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mt-lg-0 mt-5">
                    <div class="footerv2-w3ls">
                        <h3 class="mb-3 w3f_title">Tautan</h3>
                        <hr>
                        <ul class="list-agileits">
                            <li>
                                <a href="#">
                                    Dinas Kesehatan RI
                                </a>
                            </li>
                            <li class="my-2">
                                <a href="#">
                                    Dinas Kesehatan Provinsi
                                </a>
                            </li>
                            <li class="my-2">
                                <a href="#">
                                    BPJS
                                </a>
                            </li>
                            <li class="mb-2">
                                <a href="#">
                                    Peta Lokasi
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Kebijakan
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- //footer bottom -->
    </footer>
    <!-- //footer -->
    <!-- quick contact -->
    <div class="container">
        <div class="outer-col">
            <div class="heading">Login</div>
            <div class="form-col">
                <form action="<?php echo base_url('login'); ?>" method="post">
                    <input type="text" class="form-control" placeholder="Username" name="username" id="user-name" required="required">
                    <input type="password" class="form-control" placeholder="password" name="password" id="Email-id" required="required">
                    <!-- <input type="text" class="form-control" placeholder="phone number" name="Name" id="phone-number" required="">
                    <textarea placeholder="your message" class="form-control"></textarea> -->
                    <input type="submit" value="Login" class="btn_apt">
                </form>
            </div>
        </div>
    </div>
    <!-- //quick contact -->
    <!-- copyright -->
    <div class="cpy-right text-center">
        <p>© 2018 RSUD BASEMAH. All rights reserved | Design by
            <a href="http://w3layouts.com"> W3layouts.</a>
        </p>
    </div>
    <!-- //copyright -->
    <!-- js -->
    <script src="<?php echo base_url('asset/frontend/'); ?>js/jquery-2.2.3.min.js"></script>
    <!-- //js -->
    <!-- Banner Responsiveslides -->
    <script src="<?php echo base_url('asset/frontend/'); ?>js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider3").responsiveSlides({
                auto: false,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!-- //banner responsive slides -->
    <!-- Flexslider-js for-testimonials -->
    <script src="<?php echo base_url('asset/frontend/'); ?>js/jquery.flexisel.js"></script>
    <script>
        $(window).load(function () {
            $("#flexiselDemo1").flexisel({
                visibleItems: 1,
                animationSpeed: 1000,
                autoPlay: false,
                autoPlaySpeed: 3000,
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint: 480,
                        visibleItems: 1
                    },
                    landscape: {
                        changePoint: 640,
                        visibleItems: 1
                    },
                    tablet: {
                        changePoint: 768,
                        visibleItems: 1
                    }
                }
            });

        });
    </script>
    <!-- //Flexslider-js for-testimonials -->
    <!-- //fixed quick contact -->
    <script>
        $(function () {
            var hidden = true;
            $(".heading").click(function () {
                if (hidden) {
                    $(this).parent('.outer-col').animate({
                        bottom: "0"
                    }, 1200);
                } else {
                    $(this).parent('.outer-col').animate({
                        bottom: "-305px"
                    }, 1200);
                }
                hidden = !hidden;
            });
        });
    </script>
    <!-- //fixed quick contact -->
    <!-- start-smooth-scrolling -->
    <script src="<?php echo base_url('asset/frontend/'); ?>js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <script src="<?php echo base_url('asset/frontend/'); ?>js/SmoothScroll.min.js"></script>
    <!-- //end-smooth-scrolling -->
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('asset/frontend/'); ?>js/bootstrap.js"></script>
</body>

</html>