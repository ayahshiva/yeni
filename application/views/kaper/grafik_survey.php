
           <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Grafik Survey</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Bobot</strong> Nilai | Total Responden : <?php echo $responden; ?>
                                    </div>
                                    <div class="card-body card-block">
                                         <div class="col-lg-12">
                                            <div class="au-card m-b-30">
                                                <div class="au-card-inner">
                                                    <div id="container"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Nilai</strong> Rata-Rata | Total Responden : <?php echo $responden; ?>
                                    </div>
                                    <div class="card-body card-block">
                                         <div class="col-lg-12">
                                            <div class="au-card m-b-30">
                                                <div class="au-card-inner">
                                                    <div id="container2"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Nilai</strong> Per Dimensi | Total Responden : <?php echo $responden; ?>
                                    </div>
                                    <div class="card-body card-block">
                                         <div class="col-lg-12">
                                            <div class="au-card m-b-30">
                                                <div class="au-card-inner">
                                                    <div id="container3"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            
    <script src="<?php echo base_url('asset/backend/'); ?>code/highcharts.js"></script>
    <script src="<?php echo base_url('asset/backend/'); ?>code/modules/exporting.js"></script>
    <script src="<?php echo base_url('asset/backend/'); ?>code/modules/export-data.js"></script>
    <script type="text/javascript">
        Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Responden Kuisioner'
        },
        subtitle: {
            text: 'Hasil Survey Kunjungan Pasien'
        },
        xAxis: {
            min: 0,
            title: {
                text: 'Atribut Pertanyaan'
            },
            categories: [
                <?php 
                    $soal = $this->db->query("SELECT * FROM kuisioner");
                    $row = $soal->result();
                    foreach ($row as $key => $value) {
                        echo $value->id_kuis.',';
                    }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bobot Nilai'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Bobot Nilai</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: 'Persepsi',
                data: 
                [
                    <?php
                        $this->db->select('*');
                        $this->db->from('kuisioner');
                        $this->db->join('jawab_kuis', 'jawab_kuis.id_kuis = kuisioner.id_kuis');
                        $this->db->group_by('kuisioner.id_kuis');        
                        $query = $this->db->get();
                        $result = $query->result();
                        foreach ($result as $key => $value) 
                        {
                            $total = 0;
                            for($i=1; $i<=5; $i++){
                                $kueri = $this->db->query("SELECT count(p) AS p FROM jawab_kuis WHERE p='$i' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                $hasil = $kueri->num_rows($kueri);
                                $bobot = ($hasil*$i);      
                                $total = $total+$bobot;   
                            } 
                            echo $total.',';
                            //echo $value->id_kuis.',';          
                        }             
                        
                    ?>
                ]

            }, 
            {
                name: 'Ekspektasi',
                data: 
                [
                    <?php
                        $this->db->select('*');
                        $this->db->from('kuisioner');
                        $this->db->join('jawab_kuis', 'jawab_kuis.id_kuis = kuisioner.id_kuis');
                        $this->db->group_by('kuisioner.id_kuis');        
                        $query = $this->db->get();
                        $result = $query->result();
                        foreach ($result as $key => $value) 
                        {
                            $total2 = 0;
                            for($i2=1; $i2<=5; $i2++){
                                $kueri2 = $this->db->query("SELECT count(e) AS e FROM jawab_kuis WHERE e='$i2' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                $hasil2 = $kueri2->num_rows($kueri2);
                                $bobot2 = ($hasil2*$i2);      
                                $total2 = $total2+$bobot2;   
                            } 
                            echo $total2.',';         
                        }             
                        
                    ?>
                ]

            }
        ]
    });
    </script>


    <script type="text/javascript">
        Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Responden Kuisioner'
        },
        subtitle: {
            text: 'Hasil Survey Kunjungan Pasien'
        },
        xAxis: {
            min: 0,
            title: {
                text: 'Atribut Pertanyaan'
            },
            categories: [
                <?php 
                    $soal = $this->db->query("SELECT * FROM kuisioner");
                    $row = $soal->result();
                    foreach ($row as $key => $value) {
                        echo $value->id_kuis.',';
                    }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rata-Rata'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Bobot Nilai</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: 'Persepsi',
                data: 
                [
                    <?php
                        $this->db->select('*');
                        $this->db->from('kuisioner');
                        $this->db->join('jawab_kuis', 'jawab_kuis.id_kuis = kuisioner.id_kuis');
                        $this->db->group_by('kuisioner.id_kuis');        
                        $query = $this->db->get();
                        $result = $query->result();
                        foreach ($result as $key => $value) 
                        {
                            $total = 0;
                            for($i=1; $i<=5; $i++){
                                $kueri = $this->db->query("SELECT count(p) AS p FROM jawab_kuis WHERE p='$i' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                $hasil = $kueri->num_rows($kueri);
                                $bobot = ($hasil*$i);      
                                $total = $total+$bobot;   
                            } 
                            //echo $total.',';   
                            $this->db->select('id_pasien');
                            $this->db->from('jawab_kuis');
                            $this->db->group_by('id_pasien');   
                            $kueri_total = $this->db->get();
                            $hasil_total = $kueri_total->num_rows();
                            echo number_format($rata2 = $total2/$hasil_total,2).',';
                        }             
                        
                    ?>
                ]

            }, 
            {
                name: 'Ekspektasi',
                data: 
                [
                    <?php
                        $this->db->select('*');
                        $this->db->from('kuisioner');
                        $this->db->join('jawab_kuis', 'jawab_kuis.id_kuis = kuisioner.id_kuis');
                        $this->db->group_by('kuisioner.id_kuis');        
                        $query = $this->db->get();
                        $result = $query->result();
                        foreach ($result as $key => $value) 
                        {
                            $total2 = 0;
                            for($i2=1; $i2<=5; $i2++){
                                $kueri2 = $this->db->query("SELECT count(e) AS e FROM jawab_kuis WHERE e='$i2' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                $hasil2 = $kueri2->num_rows($kueri2);
                                $bobot2 = ($hasil2*$i2);      
                                $total2 = $total2+$bobot2;   
                            } 
                            //echo $total2.',';      
                            $this->db->select('id_pasien');
                            $this->db->from('jawab_kuis');
                            $this->db->group_by('id_pasien');   
                            $kueri_total = $this->db->get();
                            $hasil_total = $kueri_total->num_rows();
                            echo number_format($rata2 = $total2/$hasil_total,2).',';
                        }             
                        
                    ?>
                ]

            }
        ]
    });
    </script>

    <script type="text/javascript">
        Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Responden Kuisioner'
        },
        subtitle: {
            text: 'Hasil Survey Kunjungan Pasien'
        },
        xAxis: {
            min: 0,
            title: {
                text: 'Atribut Dimensi'
            },
            categories: [
                <?php 
                    $soal = $this->db->query("SELECT * FROM dimensi");
                    $row = $soal->result();
                    foreach ($row as $key => $value) {
                        echo $value->id_dimensi.',';
                    }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rata-Rata'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Bobot Nilai</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: 'Persepsi',
                data: 
                [
                    <?php
                        $this->db->select('*')->from('dimensi');
                        $kueri_dimensi_p = $this->db->get();
                        $hasil_dimensi_p = $kueri_dimensi_p->result();
                        foreach ($hasil_dimensi_p as $kunci_dimensi_p => $nilai_dimensi_p) 
                        {                           
                            $this->db->select('*')->from('kuisioner')->where('id_dimensi', $nilai_dimensi_p->id_dimensi);
                            $kueri_kuis_p = $this->db->get();
                            $hasil_kuis_p = $kueri_kuis_p->result();
                            foreach ($hasil_kuis_p as $kunci_kuis_p => $nilai_kuis_p) 
                            {
                                $total_p = 0;
                                for($i_p=1; $i_p<=5; $i_p++)
                                {
                                    $kueri_hitung_p = $this->db->query("SELECT count(p) AS p FROM jawab_kuis WHERE p='$i_p' AND id_kuis='$nilai_kuis_p->id_kuis' GROUP BY id_pasien ");
                                    $hasil_hitung_p = $kueri_hitung_p->num_rows();
                                    $bobot_p = $hasil_hitung_p * $i_p;
                                    $total_p = $total_p + $bobot_p;
                                }
                            }
                           //echo $total_p.',';
                            $hitung_p = $this->db->query("SELECT id_kuis, id_dimensi FROM kuisioner WHERE id_dimensi='$nilai_dimensi_p->id_dimensi'");
                            $hasil_p = $hitung_p->num_rows();
                            $rata_p = $total_p/$hasil_p;
                            echo number_format($rata_p,2).',';
                        }
                    ?>                    
                ]

            }, 
            {
                name: 'Ekspektasi',
                data: 
                [
                    <?php
                       $this->db->select('*')->from('dimensi');
                        $kueri_dimensi_e = $this->db->get();
                        $hasil_dimensi_e = $kueri_dimensi_e->result();
                        foreach ($hasil_dimensi_e as $kunci_dimensi_e => $nilai_dimensi_e) 
                        {                           
                            $this->db->select('*')->from('kuisioner')->where('id_dimensi', $nilai_dimensi_e->id_dimensi);
                            $kueri_kuis_e = $this->db->get();
                            $hasil_kuis_e = $kueri_kuis_e->result();
                            foreach ($hasil_kuis_e as $kunci_kuis_e => $nilai_kuis_e) 
                            {
                                $total_e = 0;
                                for($i_e=1; $i_e<=5; $i_e++)
                                {
                                    $kueri_hitung_e = $this->db->query("SELECT count(e) AS e FROM jawab_kuis WHERE e='$i_e' AND id_kuis='$nilai_kuis_e->id_kuis' GROUP BY id_pasien ");
                                    $hasil_hitung_e = $kueri_hitung_e->num_rows();
                                    $bobot_e = $hasil_hitung_e * $i_e;
                                    $total_e = $total_e + $bobot_e;
                                }
                            }
                            //echo $total_e.',';
                            $hitung_e = $this->db->query("SELECT id_kuis, id_dimensi FROM kuisioner WHERE id_dimensi='$nilai_dimensi_e->id_dimensi'");
                            $hasil_e = $hitung_e->num_rows();
                            $rata_e = $total_e/$hasil_e;
                            echo number_format($rata_e,2).',';
                        }
                    ?>
                ]

            }
        ]
    });
    </script>
   
