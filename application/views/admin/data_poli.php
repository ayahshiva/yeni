            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Poli</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Form</strong> Input
                                    </div>

                                    <?php
                                        if($this->uri->segment(3) != ''){
                                    ?>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_edit_poli'); ?>">
                                        <div class="card-body card-block">                                        
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Kode Poli</label>
                                                <input type="text" id="nf-email" name="kode_poli" value="<?php echo $edit_poli->kode_poli; ?>" class="form-control" required="required">
                                                <input type="hidden" name="id_poli" value="<?php echo $this->uri->segment(3); ?>">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Nama Poli</label>
                                                <input type="text" id="nf-password" value="<?php echo $edit_poli->nama_poli; ?>" name="nama_poli" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your password</span> -->
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                    <?php
                                        }else{
                                    ?>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_poli'); ?>">
                                        <div class="card-body card-block">                                        
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Kode Poli</label>
                                                <input type="text" id="nf-email" name="kode_poli" placeholder="Kode Poli" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Nama Poli</label>
                                                <input type="text" id="nf-password" name="nama_poli" placeholder="Nama Poli" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your password</span> -->
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Data</strong> Poli
                                    </div>
                                    <div class="card-body card-block" id="printableArea">
                                        <table class="table table-borderless table-data3">
                                            <thead>
                                                <tr>
                                                    <th>Kode Poli</th>
                                                    <th>Nama Poli</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($poli as $data => $datapoli) { ?>
                                                    <tr>
                                                        <td><?php echo $datapoli->kode_poli ?></td>
                                                        <td><?php echo $datapoli->nama_poli; ?></td>
                                                        <td>                                                            
                                                            <a href="<?php echo base_url('admin/data_poli/'); ?><?php echo $datapoli->id_poli; ?>">
                                                            <button type="button" class="btn btn-success btn-sm">Edit</button>
                                                            </a>
                                                            <form action="<?php echo base_url('admin/hapus_poli/'); ?><?php echo $datapoli->id_poli; ?>" method="post">
                                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?');">Hapus</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-header">
                                        <input type="button" class="btn btn-primary btn-sm" onclick="printDiv('printableArea')" value="Cetak" />
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
            <script type="text/javascript">
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
            </script>  
           
        
