            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">overview</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <h2 class="title-1 m-b-25">Kunjungan Hari Ini</h2>
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>No KTP</th>
                                                <th>Nama</th>
                                                <th>Poli</th>
                                                <th>Jaminan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($today as $key => $value) { ?>
                                            <tr>
                                                <td><?php echo $value->no_ktp; ?></td>
                                                <td><?php echo $value->nama; ?></td>
                                                <td><?php echo $value->nama_poli; ?></td>
                                                <td><?php echo $value->jaminan; ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        
