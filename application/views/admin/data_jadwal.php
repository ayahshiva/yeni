            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Jadwal</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Form</strong> Input
                                    </div>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_jadwal'); ?>">
                                        <div class="card-body card-block">                                        
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Hari</label>
                                                <select name="hari" id="select" class="form-control" required="required">
                                                    <option value="">Pilih Hari</option>
                                                    <option value="Minggu">Minggu</option>
                                                    <option value="Senin">Senin</option>
                                                    <option value="Selasa">Selasa</option>
                                                    <option value="Rabu">Rabu</option>
                                                    <option value="Kamis">Kamis</option>
                                                    <option value="Jumat">Jumat</option>
                                                    <option value="Sabtu">Sabtu</option>
                                                </select>
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Jam</label>
                                                <input type="time" id="nf-password" name="jam" placeholder="jam" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your password</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="dokter" class="form-control-label">Dokter</label>
                                                <select name="id_dokter" id="select" class="form-control" required="required">
                                                    <option value="">Pilih Dokter</option>
                                                    <?php foreach ($dokter as $data => $datadokter) { ?>
                                                        <option value="<?php echo $datadokter->id_dokter; ?>"><?php echo $datadokter->nama; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="poli" class="form-control-label">Poli</label>
                                                <select name="id_poli" id="select" class="form-control" required="required">
                                                    <option value="">Pilih Poli</option>
                                                    <?php foreach ($poli as $data => $datapoli) { ?>
                                                        <option value="<?php echo $datapoli->id_poli; ?>"><?php echo $datapoli->nama_poli; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Data</strong> Jadwal
                                    </div>
                                    <div class="card-body card-block" id="printableArea">
                                        <table class="table table-borderless table-data3">
                                            <thead>
                                                <tr>
                                                    <th>Hari</th>
                                                    <th>Jam</th>
                                                    <th>Dokter</th>
                                                    <th>Poli</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($jadwal as $key => $value) { ?>
                                                    <tr>
                                                        <td><?php echo $value->hari; ?></td>
                                                        <td><?php echo $value->jam; ?></td>                                                        
                                                        <td><?php echo $value->nama; ?></td>                                                        
                                                        <td><?php echo $value->nama_poli; ?></td>
                                                        <td>
                                                            <!-- <a href="<?php echo base_url('admin/data_jadwal/'); ?><?php echo $value->id_jadwal; ?>">
                                                            <button type="button" class="btn btn-success btn-sm">Edit</button>
                                                            </a> -->
                                                            <form action="<?php echo base_url('admin/hapus_jadwal/'); ?><?php echo $value->id_jadwal; ?>" method="post">
                                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?');">Hapus</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-header">
                                        <input type="button" class="btn btn-primary btn-sm" onclick="printDiv('printableArea')" value="Cetak" />
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->

            <script type="text/javascript">
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
            </script>  
        
        
