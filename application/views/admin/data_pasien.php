            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Pasien</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Data</strong> Pasien
                                    </div>
                                    <div class="card-body card-block" id="printableArea">
                                        <table class="table table-borderless table-data3">
                                            <thead>
                                                <tr>
                                                    <th>No KTP</th>
                                                    <th>Nama</th>
                                                    <th>Gender</th>
                                                    <th>Telepon</th>
                                                    <th>Alamat</th>
                                                    <th>Jaminan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($pasien as $key => $value) { ?>
                                                <tr>
                                                    <td><?php echo $value->no_ktp; ?></td>
                                                    <td><?php echo $value->nama; ?></td>
                                                    <td><?php echo $value->gender; ?></td>
                                                    <td><?php echo $value->telp; ?></td>
                                                    <td><?php echo $value->alamat; ?></td>
                                                    <td><?php echo $value->jaminan; ?></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="card-header">
                                        <input type="button" class="btn btn-primary btn-sm" onclick="printDiv('printableArea')" value="Cetak" />
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        <script type="text/javascript">
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
            </script>  
