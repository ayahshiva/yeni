            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Kuisioner</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Form</strong> Input
                                    </div>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_kuis'); ?>">
                                        <div class="card-body card-block">
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Dimensi</label>
                                                <select name="id_dimensi" class="form-control" required>
                                                    <option>Pilih Dimensi</option>
                                                    <?php foreach ($dimensi as $key => $value) { ?>
                                                         <option value="<?php echo $value->id_dimensi ?>"><?php echo $value->nama_dimensi; ?></option>
                                                    <?php } ?>                                                   
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Pertanyaan</label>
                                                <textarea name="soal" id="textarea-input" rows="9" placeholder="Ketik Pertanyaan disini..." class="form-control"></textarea>
                                                <!-- <input type="text" id="nf-email" name="kode_poli" placeholder="Kode Poli" class="form-control"> -->
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>                                            
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Data</strong> Kuisioner
                                    </div>
                                    <div class="card-body card-block">
                                        <table class="table table-borderless table-data3">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Dimensi</th>
                                                    <th>Pertanyaan</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = '1'; ?>
                                                <?php foreach ($kuis as $data => $datapoli) { ?>
                                                    <tr>
                                                        <td><?php echo $no++; ?></td>
                                                        <td><?php echo $datapoli->nama_dimensi; ?></td>
                                                        <td><?php echo $datapoli->soal; ?></td>
                                                        <td></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        
