            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Dokter</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Form</strong> Input
                                    </div>
                                    <?php
                                        if($this->uri->segment(3) !=''){
                                     ?>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_edit_dokter'); ?>">
                                        <div class="card-body card-block">
                                        
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">NIP</label>
                                                <input type="text" id="nf-email" name="nip" value="<?php echo $edit_dokter->nip; ?>" class="form-control" required="required">
                                                <input type="hidden" name="id_dokter" value="<?php echo $this->uri->segment(3); ?>">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Nama</label>
                                                <input type="text" id="nf-password" name="nama" value="<?php echo $edit_dokter->nama;?>" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your password</span> -->
                                            </div>
                                       
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                    <?php
                                        }else{ 
                                    ?>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_dokter'); ?>">
                                        <div class="card-body card-block">
                                        
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">NIP</label>
                                                <input type="text" id="nf-email" name="nip" placeholder="Nomor Induk Pegawai" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Nama</label>
                                                <input type="text" id="nf-password" name="nama" placeholder="Nama" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your password</span> -->
                                            </div>
                                       
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                <?php } ?>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Data</strong> Dokter
                                    </div>
                                    <div class="card-body card-block" id="printableArea">
                                        <table class="table table-borderless table-data3">
                                            <thead>
                                                <tr>
                                                    <th>NIP</th>
                                                    <th>Nama</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($dokter as $key => $value) { ?>
                                                    <tr>
                                                        <td><?php echo $value->nip; ?></td>
                                                        <td><?php echo $value->nama; ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('admin/data_dokter/'); ?><?php echo $value->id_dokter; ?>">
                                                            <button type="button" class="btn btn-success btn-sm">Edit</button>
                                                            </a>
                                                            <form action="<?php echo base_url('admin/hapus_dokter/'); ?><?php echo $value->id_dokter; ?>" method="post">
                                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?');">Hapus</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-header">
                                         <input type="button" class="btn btn-primary btn-sm" onclick="printDiv('printableArea')" value="Cetak" />
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->

            <script type="text/javascript">
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
            </script>  
        
