                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Nilai</strong> Per Dimensi
                                    </div>
                                    <div class="card-body card-block">
                                        <table class="table table-borderless table-data1">
                                            <thead>
                                                <tr>
                                                    <th>Dimensi Pernyataan</th>
                                                    <th>Atribut Pernyataan</th>
                                                    <th>Nilai Bobot Persepsi</th>
                                                    <th>Rata-Rata Persepsi</th>
                                                    <th>Nilai Bobot Ekspektasi</th>
                                                    <th>Rata-Rata Ekspektasi</th>
                                                    <th>Nilai Gap 5</th>
                                                </tr>
                                            </thead>     
                                            <tbody>
                                                <?php foreach ($dimensi as $key => $value) { ?>
                                                    <tr>
                                                        <td><?php echo $value->nama_dimensi; ?></td>
                                                        <td>
                                                            <?php
                                                                $this->db->select('*')->from('kuisioner')->where('id_dimensi', $value->id_dimensi);
                                                                $query = $this->db->get();
                                                                $result = $query->result();
                                                                foreach ($result as $key => $value) {
                                                                    echo $value->id_kuis.' ';
                                                                }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                                $this->db->select('*')->from('kuisioner')->where('id_dimensi', $value->id_dimensi);
                                                                $query = $this->db->get();
                                                                $result = $query->result();
                                                                foreach ($result as $key => $value) {
                                                                    $total_p = 0;
                                                                    for($i=1; $i<=5; $i++)
                                                                    {
                                                                        $kueri_p = $this->db->query("SELECT count(p) AS p FROM jawab_kuis WHERE p='$i' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                                                        $hasil_p = $kueri_p->num_rows();
                                                                        $bobot_p = $hasil_p*$i;
                                                                        $total_p = $total_p+$bobot_p; 
                                                                    }                                                                    
                                                                }
                                                                echo $total_p;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                                $hitung = $this->db->query("SELECT id_kuis, id_dimensi FROM kuisioner WHERE id_dimensi='$value->id_dimensi'");
                                                                $hasil = $hitung->num_rows();
                                                                $rata_p = $total_p/$hasil;
                                                                echo number_format($rata_p,2,",",".");
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                                $this->db->select('*')->from('kuisioner')->where('id_dimensi', $value->id_dimensi);
                                                                $query_e = $this->db->get();
                                                                $result_e = $query_e->result();
                                                                foreach ($result_e as $key => $value) {
                                                                    $total_e = 0;
                                                                    for($ie=1; $ie<=5; $ie++)
                                                                    {
                                                                        $kueri_e = $this->db->query("SELECT count(e) AS e FROM jawab_kuis WHERE e='$ie' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                                                        $hasil_e = $kueri_e->num_rows();
                                                                        $bobot_e = $hasil_e*$ie;
                                                                        $total_e = $total_e+$bobot_e; 
                                                                    }                                                                    
                                                                }
                                                                echo $total_e;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                                $hitung_e = $this->db->query("SELECT id_kuis, id_dimensi FROM kuisioner WHERE id_dimensi='$value->id_dimensi'");
                                                                $hasil_e = $hitung_e->num_rows();
                                                                $rata_e = $total_e/$hasil_e;
                                                                echo number_format($rata_e,2,",",".");
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                                echo number_format($rata_e-$rata_p,2,",",".");
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>                                                                                 
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>