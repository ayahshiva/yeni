                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Nilai</strong> Rata-Rata
                                    </div>
                                    <div class="card-body card-block">
                                        <table class="table table-borderless table-data1">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2">atribut<br>pertanyaan</th>
                                                    <th colspan="2">persepsi</th>
                                                    <th colspan="2">ekspektasi</th>
                                                    <th>Nilai Gap<br>5</th>
                                                </tr>
                                                <tr>
                                                    <td>bobot nilai</td>
                                                    <td>rata-rata</td>
                                                    <td>bobot nilai</td>
                                                    <td>rata-rata</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($hasil as $key => $value) { ?>
                                                <tr>
                                                    <td><?php echo $value->id_kuis; ?></td>
                                                    <td>
                                                        <?php
                                                            $total = 0;
                                                            for($i=1; $i<=5; $i++){
                                                                $kueri = $this->db->query("SELECT count(p) AS p FROM jawab_kuis WHERE p='$i' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                                                $hasil = $kueri->num_rows($kueri);
                                                                $bobot = ($hasil*$i);      
                                                                $total = $total+$bobot;   
                                                            } 
                                                            echo $total;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            echo number_format($rata2 = $total/$responden,2,",",".");
                                                        ?>                                            
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $total2 = 0;
                                                            for($i2=1; $i2<=5; $i2++){
                                                                $kueri2 = $this->db->query("SELECT count(e) AS e FROM jawab_kuis WHERE e='$i2' AND id_kuis='$value->id_kuis' GROUP BY id_pasien");
                                                                $hasil2 = $kueri2->num_rows($kueri2);
                                                                $bobot2 = ($hasil2*$i2);      
                                                                $total2 = $total2+$bobot2;   
                                                            } 
                                                            echo $total2;
                                                        ?>   
                                                    </td>
                                                    <td>
                                                        <?php echo number_format($rata22 = $total2/$responden,2,",","."); ?>
                                                    </td>
                                                    <td>
                                                        <?php echo number_format($rata22-$rata2,2,",","."); ?>
                                                    </td>
                                                </tr>             
                                            <?php } ?>                                  
                                            </tbody>                                           
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>