            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Hasil Survey</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Responden</strong> Kuisioner
                                    </div>
                                    <div class="card-body card-block">
                                        <table class="table table-borderless table-data1">
                                            <thead>
                                                <tr>
                                                   <tr>
                                                    <th rowspan="2">No</th>
                                                    <th rowspan="2">Pertanyaan</th>
                                                    <th colspan="5">Persepsi</th>
                                                    <th></th>
                                                    <th colspan="5">Ekspektasi</th>
                                                </tr>
                                                <tr>
                                                    <th>1</th>
                                                    <th>2</th>
                                                    <th>3</th>
                                                    <th>4</th>
                                                    <th>5</th>
                                                    <th></th>
                                                    <th>1</th>
                                                    <th>2</th>
                                                    <th>3</th>
                                                    <th>4</th>
                                                    <th>5</th>
                                                </tr>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($hasil as $key => $value) { ?>
                                                <tr>
                                                    <td><?php echo $value->id_kuis; ?></td>
                                                    <td style="text-align: justify;"><?php echo $value->soal; ?></td>
                                                    <td>
                                                        <?php
                                                            $kuerip1 = $this->db->query("SELECT count(p) as p1 FROM jawab_kuis WHERE p='1'  GROUP BY id_pasien");
                                                            $p1_a = $kuerip1->num_rows();                                                    
                                                            echo $p1_a; 
                                                        ?>                                                        
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerip2 = $this->db->query("SELECT count(p) as p2 FROM jawab_kuis WHERE p='2'  GROUP BY id_pasien");
                                                            $p2_a = $kuerip2->num_rows();                                                    
                                                            echo $p2_a; 
                                                        ?>   
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerip3 = $this->db->query("SELECT count(p) as p3 FROM jawab_kuis WHERE p='3'  GROUP BY id_pasien");
                                                            $p3_a = $kuerip3->num_rows();                                                    
                                                            echo $p3_a; 
                                                        ?>   
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerip4 = $this->db->query("SELECT count(p) as p4 FROM jawab_kuis WHERE p='4'  GROUP BY id_pasien");
                                                            $p4_a = $kuerip4->num_rows();                                                    
                                                            echo $p4_a; 
                                                        ?>   
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerip5 = $this->db->query("SELECT count(p) as p5 FROM jawab_kuis WHERE p='5'  GROUP BY id_pasien");
                                                            $p5_a = $kuerip5->num_rows();                                                    
                                                            echo $p5_a; 
                                                        ?>   
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                         <?php
                                                            $kuerie1 = $this->db->query("SELECT count(e) as e1 FROM jawab_kuis WHERE e='1'  GROUP BY id_pasien");
                                                            $e1_a = $kuerie1->num_rows();                                                    
                                                            echo $e1_a; 
                                                        ?>   
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerie2 = $this->db->query("SELECT count(e) as e2 FROM jawab_kuis WHERE e='2'  GROUP BY id_pasien");
                                                            $e2_a = $kuerie2->num_rows();                                                    
                                                            echo $e2_a; 
                                                        ?>   
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerie3 = $this->db->query("SELECT count(e) as e3 FROM jawab_kuis WHERE e='3'  GROUP BY id_pasien");
                                                            $e3_a = $kuerie3->num_rows();                                                    
                                                            echo $e3_a; 
                                                        ?>   
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerie4 = $this->db->query("SELECT count(e) as e4 FROM jawab_kuis WHERE e='4'  GROUP BY id_pasien");
                                                            $e4_a = $kuerie4->num_rows();                                                    
                                                            echo $e4_a; 
                                                        ?>   
                                                    </td>
                                                    <td>
                                                         <?php
                                                            $kuerie5 = $this->db->query("SELECT count(e) as e5 FROM jawab_kuis WHERE e='5'  GROUP BY id_pasien");
                                                            $e5_a = $kuerie5->num_rows();                                                    
                                                            echo $e5_a; 
                                                        ?>   
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>                                           
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <?php $this->load->view('admin/hasil_rata_rata'); ?>

                        <?php $this->load->view('admin/per_dimensi'); ?>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        
