            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Data Pengguna</h2>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row m-t-25">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Form</strong> Input
                                    </div>

                                    <?php
                                        if($this->uri->segment(3) != '')
                                        {
                                    ?>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_edit_pengguna'); ?>">
                                        <div class="card-body card-block">
                                        
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Username</label>
                                                <input type="hidden" name="id_login" value="<?php echo $this->uri->segment(3); ?>">
                                                <input type="text" id="nf-email" name="username" value="<?php echo $edit->username; ?>" " class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Email</label>
                                                <input type="text" id="nf-email" name="email" value="<?php echo $edit->email; ?>" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Hak Akses</label>
                                                <select name="hak_akses" class="form-control" required>
                                                    <option value="">Pilih Hak Akses</option>
                                                    <option value="1">Admin</option>
                                                    <option value="3">Kepala Perawat</option>
                                                </select>
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                    <?php
                                        }else{
                                    ?>
                                    <form method="post" class="" action="<?php echo base_url('admin/simpan_pengguna'); ?>">
                                        <div class="card-body card-block">
                                        
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Username</label>
                                                <input type="text" id="nf-email" name="username" placeholder="Username" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Email</label>
                                                <input type="text" id="nf-email" name="email" placeholder="Email" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Password</label>
                                                <input type="password" id="nf-email" name="password" placeholder="password" class="form-control" required="required">
                                                <!-- <span class="help-block">Please enter your email</span> -->
                                            </div>
                                            <div class="form-group">
                                                <label for="nf-password" class=" form-control-label">Hak Akses</label>
                                                <select name="hak_akses" class="form-control" required>
                                                    <option>Pilih Hak Akses</option>
                                                    <option value="1">Admin</option>
                                                    <option value="2">Kepala Perawat</option>
                                                </select>
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>                                        
                                        </div>
                                    </form>
                                <?php } ?>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Data</strong> Pengguna
                                    </div>
                                    <div class="card-body card-block" id="printableArea">
                                        <table class="table table-borderless table-data3">
                                            <thead>
                                                <tr>
                                                    <th>Username</th>
                                                    <th>Email</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($pengguna as $data => $datapoli) { ?>
                                                    <tr>
                                                        <td><?php echo $datapoli->username ?></td>
                                                        <td><?php echo $datapoli->email; ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('admin/data_pengguna/'); ?><?php echo $datapoli->id_login; ?>">
                                                            <button type="button" class="btn btn-success btn-sm">Edit</button>
                                                            </a>
                                                            <form action="<?php echo base_url('admin/hapus_pengguna/'); ?><?php echo $datapoli->id_login; ?>" method="post">
                                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?');">Hapus</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-header">
                                       <input type="button" class="btn btn-primary btn-sm" onclick="printDiv('printableArea')" value="Cetak" />
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
            <script type="text/javascript">
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
            </script>  
           