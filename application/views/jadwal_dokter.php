
    <!-- breadcrumbs -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Beranda</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Jadwal Dokter</li>
        </ol>
    </nav>
    <!-- //breadcrumbs -->
    <!-- //banner -->
   
    <section class="wthree-row pt-3 pb-lg-5 w3-contact">
        <div class="container py-sm-5 pt-0 pb-5">
            <div class="row contact-form pt-lg-5">
                <!-- contact details -->
                <div class="col-lg-4 contact-bottom mt-lg-0 mt-5">
                    <div class="contact-details-top">
                       <h5 class="sub-title-wthree">Hubungi Kami</h5>
                        <div class="row wthree-cicon">
                            <span class="fas fa-envelope-open mr-3"></span>
                            <a href="mailto:info@example.com">info@rsud-basemah.com</a>
                        </div>
                        <div class="row wthree-cicon">
                            <span class="fas fa-phone-volume mr-3"></span>
                            <h6>0711 717171</h6>
                        </div>
                        <div class="row wthree-cicon">
                            <span class="fas fa-globe mr-3"></span>
                            <a href="#">www.rsud-basemah.com</a>
                        </div>
                    </div>
                    <br />
                    <div class="footerv2-w3ls">
                        <h5 class="sub-title-wthree">sosial media</h5>
                        <ul class="social-iconsv2 agileinfo">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- //contact details -->
                <div class="col-lg-8 wthree-form-left px-lg-5 mt-lg-0 mt-5">
                    <!-- contact form grid -->
                    <div class="contact-top1">
                        <h5 class="sub-title-wthree">Jadwal Dokter</h5>
                            <table width="100%" border="1">
                                <thead>
                                    <tr>
                                        <th>Hari</th><th>Jam</th><th>Dokter</th><th>Poli</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($jadwal as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value->hari; ?></td>
                                            <td><?php echo $value->jam; ?></td>
                                            <td><?php echo $value->nama; ?></td>
                                            <td><?php echo $value->nama_poli; ?></td>
                                    <?php } ?>
                                </tbody>
                            </table>
                    </div>
                    <!--  //contact form grid ends here -->
                </div>
            </div>
            <!-- //contact details container -->
        </div>
    </section>
    