<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('user_model');
	}

	function add()
	{
		//$nip = '010101010101';
		$username = 'SuperAdmin';
		$email = 'superadmin@website.com';
		$password = 'SuperAdmin1234';
		$role = '1';
		$hash = $this->bcrypt->hash_password($password);

		$data = array(
			//'nip' => $nip,
			'username' => $username,
			'email' => $email,
			'password' => $hash,
			'role' => $role,
		);
		$this->admin_model->add($data);
		redirect('home');
	}

	public function index()
	{
		$this->load->view('header');
		//$this->load->view('top_menu');
		$this->load->view('index');
		$this->load->view('footer');
	}

	public function poliklinik()
	{
		$this->load->view('header');
		$this->load->view('poliklinik');
		$this->load->view('footer');
	}

	public function profil()
	{
		$this->load->view('header');
		$this->load->view('profil');
		$this->load->view('footer');
	}

	public function jadwal_dokter()
	{
		$data['dokter'] = $this->admin_model->data_dokter()->result();
		$data['jadwal'] = $this->admin_model->data_jadwal()->result();

		$this->load->view('header');
		$this->load->view('jadwal_dokter', $data);
		$this->load->view('footer');
	}

	public function Daftar()
	{
		$this->load->view('header');
		//$this->load->view('top_menu');
		$this->load->view('form_daftar');
		$this->load->view('footer');
	}

	public function Pengaduan()
	{
		$this->load->view('header');
		$this->load->view('form_pengaduan');
		$this->load->view('footer');
	}

	function simpan_pengaduan()
	{
		$input = $this->input->post(NULL, TRUE);
		$data = array(
			'tanggal' => $input['tgl'],
			'nama' => $input['nama'],
			'email' => $input['email'],
			'judul' => $input['judul'],
			'komentar' => $input['isi'],
			'status' => '1',
		);
		$this->admin_model->simpan_pengaduan($data);
		redirect('home/Pengaduan', 'refresh');
	}

	function simpan_daftar()
	{
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$role = '2';
		$hash = $this->bcrypt->hash_password($password);

		$data = array(
			'username' => $username,
			'email' => $email,
			'password' => $hash,
			'role' => $role,
		);
		$this->user_model->simpan_daftar($data);
		redirect('home');
	}
	
	function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$result = $this->user_model->cek_login($username);
		if($result == TRUE){
			$data_admin = array(
				'id_login' => $result['id_login'],
				'username' => $result['username'],
				'email' => $result['email'],
				'password' => $result['password'],
			);
			$this->session->set_userdata($data_admin);
			$stored_hash = $this->session->userdata('password');
			if($this->bcrypt->check_password($password, $stored_hash)){

				$id_login = $this->session->userdata('id_login');

				$cek = $this->user_model->cek_data('id_login')->row();

				if($cek->id_login < 1 ){
					$data = array(
						'no_rm' => 'RM'.date('ymdhis'),
						'id_login' => $this->session->userdata('id_login'),
						'survey' => '1',
						'tanggal' => date('Y-m-d'),
						);
					$this->user_model->simpan_profil($data);
					redirect('user');
				}else{
					redirect('user');
				}
				
			}else{
				$this->session->set_flashdata('error','username dan/atau Password Salah!!!');
				redirect('home');
			}
		}else{
			$this->session->set_flashdata('none', 'Username Tidak Terdaftar!!!');
			redirect('home');
		}
	}
}
