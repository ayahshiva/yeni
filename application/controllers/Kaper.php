<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kaper extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('user_model');
	}

	function Logout()
	{
		$this->session->unset_userdata('id_login');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('password');

		redirect('admin');
	}

	function dashboard()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{	
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['today'] = $this->admin_model->today()->result();
			
			$this->load->view('kaper/header', $data);
			$this->load->view('kaper/dashboard', $data);
			$this->load->view('kaper/footer', $data);
		}
	}

	function data_jadwal()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['jadwal'] = $this->admin_model->data_jadwal()->result();
			$data['poli'] = $this->admin_model->data_poli()->result();
			$data['dokter'] = $this->admin_model->data_dokter()->result();

			$this->load->view('kaper/header', $data);
			$this->load->view('kaper/data_jadwal', $data);
			$this->load->view('kaper/footer', $data);
		}
	}

	function data_pasien()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['pasien'] = $this->admin_model->data_pasien()->result();

			$this->load->view('kaper/header', $data);
			$this->load->view('kaper/data_pasien', $data);
			$this->load->view('kaper/footer', $data);
		}
	}

	function data_kunjungan()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['kunjungan'] = $this->admin_model->data_kunjungan()->result();

			$this->load->view('kaper/header', $data);
			$this->load->view('kaper/data_kunjungan', $data);
			$this->load->view('kaper/footer', $data);
		}
	}

	function hasil_survey()
	{
		if($this->session->userdata('id_login') == '')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['hasil'] = $this->admin_model->hasil_survey()->result();
			$data['responden'] = $this->admin_model->total_responden()->num_rows();
			$data['dimensi'] = $this->admin_model->tabel_dimensi()->result();

			$this->load->view('kaper/header', $data);
			$this->load->view('kaper/hasil_survey', $data);
			$this->load->view('kaper/footer', $data);

		}
	}

	function grafik_survey()
	{
		if($this->session->userdata('id_login') == '')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['hasil'] = $this->admin_model->hasil_survey()->result();
			$data['responden'] = $this->admin_model->total_responden()->num_rows();

			$this->load->view('kaper/header', $data);
			$this->load->view('kaper/grafik_survey', $data);
			$this->load->view('kaper/footer', $data);

		}
	}
}