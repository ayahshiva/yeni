<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('user_model');
	}

	function index()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$result = $this->admin_model->cek_login($username);
		if($result == TRUE){
			$data_admin = array(
				'id_login' => $result['id_login'],
				'role' => $result['role'],
				'username' => $result['username'],
				'email' => $result['email'],
				'password' => $result['password'],
			);
			$this->session->set_userdata($data_admin);
			$stored_hash = $this->session->userdata('password');
			if($this->bcrypt->check_password($password, $stored_hash)){
				
				if($this->session->userdata('role') == '1'){
					redirect('admin/dashboard');	
				}elseif($this->session->userdata('role') == '3'){
					redirect('kaper/dashboard');
				}elseif($this->session->userdata('role') == '2'){
					$id_login = $this->session->userdata('id_login');
					//$cek = $this->user_model->cek_data('id_login')->row();

					$idlogin = $this->session->userdata('id_login');
					$kueri = $this->db->query("SELECT * FROM pasien WHERE id_login='$idlogin' LIMIT 1");
					$hasil = $kueri->row();

					if($hasil < 1 ){
						$data = array(
							'no_rm' => 'RM'.date('ymdhis'),
							'id_login' => $this->session->userdata('id_login'),
							'survey' => '1',
							'tanggal' => date('Y-m-d'),
							);
						$this->user_model->simpan_profil($data);
						redirect('user');
					}else{
						redirect('user');
					}
				}				
			}else{
				$this->session->set_flashdata('error','Username dan/atau Password Salah!!!');
				redirect('admin');
			}
		}else{
			$this->session->set_flashdata('none', 'Username Tidak Terdaftar');
			redirect('admin');
		}
	}
}