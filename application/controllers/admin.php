<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('user_model');
	}

	function index()
	{
		$this->load->view('admin/login');
	}

	function add()
	{
		//$nip = '010101010101';
		$username = 'admin';
		$email = 'admin@website.com';
		$password = 'admin1234';
		$role = '1';
		$hash = $this->bcrypt->hash_password($password);

		$data = array(
			//'nip' => $nip,
			'username' => $username,
			'email' => $email,
			'password' => $hash,
			'role' => $role,
		);
		$this->admin_model->add($data);
		redirect('admin');
	}

	function cek_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$result = $this->admin_model->cek_login($username);
		if($result == TRUE){
			$data_admin = array(
				'id_login' => $result['id_login'],
				'role' => $result['role'],
				'username' => $result['username'],
				'email' => $result['email'],
				'password' => $result['password'],
			);
			$this->session->set_userdata($data_admin);
			$stored_hash = $this->session->userdata('password');
			if($this->bcrypt->check_password($password, $stored_hash)){
				redirect('admin/dashboard');
			}else{
				$this->session->set_flashdata('error','Username dan/atau Password Salah!!!');
				redirect('admin');
			}
		}else{
			$this->session->set_flashdata('none', 'Username Tidak Terdaftar');
			redirect('admin');
		}
	}

	function Logout()
	{
		$this->session->unset_userdata('id_login');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('password');

		redirect('admin');
	}

	function dashboard()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{	
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['today'] = $this->admin_model->today()->result();
			
			$this->load->view('admin/header', $data);
			$this->load->view('admin/dashboard', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function data_pengguna()
	{
		if($this->session->userdata('id_login') == '')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['pengguna'] = $this->admin_model->data_pengguna()->result();

			$id_edit = $this->uri->segment(3);
			$data['edit'] = $this->admin_model->id_edit($id_edit)->row();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/data_pengguna', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function simpan_pengguna()
	{
		$input = $this->input->post(NULL,TRUE);
		$password = $input['password'];
		$hash = $this->bcrypt->hash_password($password);

		$data = array(
			'username' => $input['username'],
			'password' => $hash,
			'email' => $input['email'],
			'role' => '3',
		);
		$this->admin_model->add($data);
		redirect('admin/data_pengguna', 'refresh');
	}

	function simpan_edit_pengguna()
	{
		$id_login = $this->input->post('id_login');
		$input = $this->input->post(NULL, TRUE);
		$data = array(
				"id_login" => $input['id_login'],
				"username" => $input['username'],
				"email" => $input['email'],
				"role" => $input['hak_akses'],
			);
		$this->admin_model->simpan_edit_pengguna($data, $id_login);
		redirect('admin/data_pengguna', 'refresh');
	}

	function hapus_pengguna()
	{
		$id_login = $this->uri->segment(3);
		$this->db->where('id_login', $id_login);
		$this->db->delete('login');
		redirect('admin/data_pengguna', 'refresh');
	}

	function data_poli()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['poli'] = $this->admin_model->data_poli()->result();
			$id_poli = $this->uri->segment(3);
			$data['edit_poli'] = $this->admin_model->edit_poli($id_poli)->row();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/data_poli', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function simpan_edit_poli()
	{
		$id_poli = $this->input->post('id_poli');
		$input = $this->input->post(NULL, TRUE);
		$data = array('kode_poli' => $input['kode_poli'], 'nama_poli' => $input['nama_poli']);
		$this->admin_model->simpan_edit_poli($data, $id_poli);
		redirect('admin/data_poli', 'refresh');
	}

	function simpan_poli()
	{
		$input = $this->input->post(NULL, TRUE);
		$data = array(
			'kode_poli' => $input['kode_poli'],
			'nama_poli' => $input['nama_poli'],
			'status' => '1',
		);
		$this->admin_model->simpan_poli($data);
		redirect('admin/data_poli', 'refresh');
	}

	function hapus_poli()
	{
		$id_poli = $this->uri->segment(3);
		$this->db->where('id_poli', $id_poli);
		$this->db->delete('poli');
		redirect('admin/data_poli', 'refresh');
	}

	function data_dokter()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['dokter'] = $this->admin_model->data_dokter()->result();
			$id_dokter = $this->uri->segment(3);
			$data['edit_dokter'] = $this->admin_model->id_dokter($id_dokter)->row();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/data_dokter', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function simpan_dokter()
	{
		$input = $this->input->post(NULL, TRUE);
		$data = array(
			'nip' => $input['nip'],
			'nama' => $input['nama'],
			'status' => '1',
		);
		$this->admin_model->simpan_dokter($data);
		redirect('admin/data_dokter', 'refresh');
	}

	function simpan_edit_dokter()
	{
		$id_dokter = $this->input->post('id_dokter');
		$input = $this->input->post(NULL, TRUE);
		$data = array('nip' => $input['nip'], 'nama' => $input['nama']);
		$this->admin_model->simpan_edit_dokter($data, $id_dokter);
		redirect('admin/data_dokter', 'refresh');
	}

	function hapus_dokter()
	{
		$id_dokter = $this->uri->segment(3);
		$this->db->where('id_dokter', $id_dokter);
		$this->db->delete('dokter');
		redirect('admin/data_dokter', 'refresh');
	}

	function data_jadwal()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['jadwal'] = $this->admin_model->data_jadwal()->result();
			$data['poli'] = $this->admin_model->data_poli()->result();
			$data['dokter'] = $this->admin_model->data_dokter()->result();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/data_jadwal', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function simpan_jadwal()
	{
		$input = $this->input->post(NULL, TRUE);
		$data = array(
			'id_poli' => $input['id_poli'],
			'id_dokter' => $input['id_dokter'],
			'hari' => $input['hari'],
			'jam' => $input['jam'],
			'status' => '1',
		);
		$this->admin_model->simpan_jadwal($data);
		redirect('admin/data_jadwal', 'refresh');
	}

	function hapus_jadwal()
	{
		$id_jadwal = $this->uri->segment(3);
		$this->db->where('id_jadwal', $id_jadwal);
		$this->db->delete('jadwal');
		redirect('admin/data_jadwal', 'refresh');
	}

	function data_pasien()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['pasien'] = $this->admin_model->data_pasien()->result();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/data_pasien', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function data_kunjungan()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['kunjungan'] = $this->admin_model->data_kunjungan()->result();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/data_kunjungan', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function data_pengaduan()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['pengaduan'] = $this->admin_model->data_pengaduan()->result();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/data_pengaduan', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function input_dimensi()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['dimensi'] = $this->admin_model->data_dimensi()->result();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/input_dimensi', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function simpan_dimensi()
	{
		$input = $this->input->post(NULL, TRUE);
		$data = array('nama_dimensi' => $input['nama_dimensi']);
		$this->admin_model->simpan_dimensi($data);
		redirect('admin/input_dimensi', 'refresh');
	}

	function input_kuis()
	{
		if($this->session->userdata('id_login') =='')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['kuis'] = $this->admin_model->data_kuis()->result();
			$data['dimensi'] = $this->admin_model->data_dimensi()->result();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/input_kuis', $data);
			$this->load->view('admin/footer', $data);
		}
	}

	function simpan_kuis()
	{
		$input = $this->input->post(NULL, TRUE);
		$data = array('soal' => $input['soal'], 'id_dimensi' => $input['id_dimensi']);
		$this->admin_model->simpan_kuis($data);
		redirect('admin/input_kuis', 'refresh');
	}

	function hasil_survey()
	{
		if($this->session->userdata('id_login') == '')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['hasil'] = $this->admin_model->hasil_survey()->result();
			$data['responden'] = $this->admin_model->total_responden()->num_rows();
			$data['dimensi'] = $this->admin_model->tabel_dimensi()->result();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/hasil_survey', $data);
			$this->load->view('admin/footer', $data);

		}
	}

	function grafik_survey()
	{
		if($this->session->userdata('id_login') == '')
		{
			redirect('admin');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['login'] = $this->admin_model->cek_data($id_login)->row();
			$data['hasil'] = $this->admin_model->hasil_survey()->result();
			$data['responden'] = $this->admin_model->total_responden()->num_rows();

			$this->load->view('admin/header', $data);
			$this->load->view('admin/grafik_survey', $data);
			$this->load->view('admin/footer', $data);

		}
	}

	function download()
	{

    	$data = file_get_contents(APPPATH . 'uploads/dokumen/'.$this->uri->segment(3)); // Read the file's contents
    	$name = $this->uri->segment(3);
    	force_download($name, $data);
	}
	
}