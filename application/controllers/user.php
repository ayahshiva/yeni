<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('user_model');
	}

	function index()
	{

		$id_login = $this->session->userdata('id_login');
		$data['user'] = $this->user_model->cek_data($id_login)->row();

		$query = $this->user_model->cek_pasien($id_login)->row();
		$no_rm = $query->no_rm;
		$data['id_pasien'] = $id_pasien = $query->id_pasien; 

		$data['kueri'] = $this->user_model->data_kunjungan($id_pasien)->result();
		$data['survey'] = $this->user_model->cek_survey($id_pasien)->row();

		$data['soal'] = $this->user_model->soal()->result();

		$this->load->view('user/header');
		$this->load->view('user/dashboard', $data);
		$this->load->view('user/footer');
	}

	function logout()
	{
		$this->session->unset_userdata('id_login');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('password');

		redirect('home');
	}

	function profil()
	{
		if($this->session->userdata('id_login') == '')
		{
			redirect('home');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['user'] = $this->user_model->cek_data($id_login)->row();
			//$data['query'] = $this->admin_model->daftar();

			$this->load->view('user/header', $data);
			$this->load->view('user/form_profil', $data);
			$this->load->view('user/footer');
		}
	}

	function ubah_password()
	{
		$id_ins = $this->input->post('id_ins');
		$password = $this->input->post('password');
		$hash = $this->bcrypt->hash_password($password);

		$data = array(
			"password" => $hash,
		);
		$this->user_model->ubah_password($data, $id_ins);
		$this->session->set_flashdata('ubah', 'Password anda telah diubah, silahkan login kembali dengan password baru anda');
		redirect('home/daftar#daftar');
	}

	function update_profil()
	{
		$id_login = $this->input->post('id_login');
		$input = $this->input->post(NULL, TRUE);
		$data = array(
				'no_ktp' => $input['no_ktp'],
				'nama' => $input['nama'],
				'gender' => $input['gender'],
				'tgl_lahir' => $input['tgl_lahir'],
				'telp' => $input['telp'],
				'alamat' => $input['alamat'],
				'pekerjaan' => $input['pekerjaan'],
				'jaminan' => $input['jaminan'],
				);
		$this->user_model->update_profil($data, $id_login);
		$this->session->set_flashdata('sukses','Profil anda telah diupdate');
		redirect('user/profil', 'refresh');
	}

	function rawat_jalan()
	{
		if($this->session->userdata('id_login') == '')
		{
			redirect('home');
		}
		else
		{
			$id_login = $this->session->userdata('id_login');
			$data['user'] = $this->user_model->cek_data($id_login)->row();

			$data['poli'] = $this->user_model->data_poli()->result();

			$this->load->view('user/header', $data);
			$this->load->view('user/rawat_jalan', $data);
			$this->load->view('user/footer');

		}
	}

	function simpan_rekam_medis()
	{
		$input = $this->input->post(NULL, TRUE);
		$data = array(
				'no_rm' => $input['no_rm'],
				'id_pasien' => $input['id_pasien'],
				'tgl_daftar' => $input['tgl_daftar'],
				'id_poli' => $input['id_poli'],
				'jmn' => $input['jaminan'],
				'status' => '1',
			);
		$this->user_model->simpan_rekam_medis($data);
		$this->session->set_flashdata('simpan','Data Anda Telah Disimpan');
		redirect('user');
	}

	function simpan_survey()
	{
		//$input = $this->input->post(NULL, TRUE);

		$id_kuis = $this->input->post('id_kuis');
		$id_pasien = $this->input->post('id_pasien');
		$p = $this->input->post('p');
		$e = $this->input->post('e');

		foreach ($id_kuis as $kuis => $idkuis) {
			$data = array(
				'id_kuis' => $idkuis,
				'id_pasien' => $id_pasien[$kuis],
				'p' => $p[$idkuis],
				'e' => $e[$idkuis],
			);
			$this->user_model->simpan_survey($data);	
		}
		$id_login = $this->session->userdata('id_login');
		redirect('user/ubah_survey/'.$id_login);		
	}

	function ubah_survey($id_login)
	{
		$this->db->query("UPDATE pasien SET survey='2' WHERE id_login=$id_login");
		redirect('user', 'refresh');
	}	

	function cetak_kartu()
	{

		$id_login = $this->session->userdata('id_login');
		$data['user'] = $this->user_model->cek_data($id_login)->row();
		
		$this->load->view('user/header', $data);
		$this->load->view('user/cetak_kartu', $data);
		$this->load->view('user/footer');
	}
}