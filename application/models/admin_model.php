<?php
class Admin_Model extends CI_Model
{
	function main()
	{
		parent::__construct();			
	}

	function cek_email($email){
		$this->db->where('email', $email);
		$cek = $this->db->get('instansi');
		if($cek->num_rows() > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function add($array_data) {
        $this->db->insert('login', $array_data);
   }

   function cek_login($username){
   		$query = $this
   			->db
   			->where('username', $username)
   			->get('login');
   		if($query->num_rows() == 1){
   			return $query->row_array();
   		}else{
   			return FALSE;
   		}
   }

   function cek_data($id_login)
   {
  		$this->db->select('*')->from('login')->where('id_login', $id_login);
  		return $this->db->get();
   }

   function ubah_password($data, $id_ptgs)
   {
   		$this->db->where('id_ptgs', $id_ptgs);
   		$this->db->update('petugas', $data);
   }

   function today()
   {
      $tgl = date('Y-m-d');
      $this->db->select('*');
      $this->db->from('rekam_medis');
      $this->db->join('pasien', 'pasien.id_pasien = rekam_medis.id_pasien');
      $this->db->join('poli', 'poli.id_poli = rekam_medis.id_poli');
      $this->db->where('rekam_medis.tgl_daftar', $tgl);
      return $this->db->get();
   }

   function data_poli()
   {
      $this->db->select('*')->from('poli');
      return $this->db->get();
   }

   function simpan_poli($data)
   {
      $this->db->insert('poli', $data);
   }

   function edit_poli($id_poli)
   {
      $this->db->select('*')->from('poli')->where('id_poli', $id_poli)->limit(1);
      return $this->db->get();
   }

   function simpan_edit_poli($data, $id_poli)
   {
      $this->db->where('id_poli', $id_poli);
      $this->db->update('poli', $data);
   }

   function data_pengguna()
   {
      $this->db->select('*')->from('login')->where('role','1')->or_where('role','3');
      return $this->db->get();
   }

   function simpan_edit_pengguna($data, $id_login)
   {
      $this->db->where('id_login', $id_login);
      $this->db->update('login', $data);
   }

   function id_edit($id_edit)
   {
      $this->db->select('*')->from('login')->where('id_login', $id_edit);
      return $this->db->get();
   }

   function data_dokter()
   {
      $this->db->select('*')->from('dokter');
      return $this->db->get();
   }

   function simpan_dokter($data)
   {
      $this->db->insert('dokter', $data);
   }

   function id_dokter($id_dokter)
   {
      $this->db->select('*')->from('dokter')->where('id_dokter', $id_dokter)->limit(1);
      return $this->db->get();
   }

   function simpan_edit_dokter($data, $id_dokter)
   {
      $this->db->where('id_dokter', $id_dokter);
      $this->db->update('dokter', $data);
   }

   function data_jadwal()
   {
      $this->db->select('*');
      $this->db->from('jadwal');
      $this->db->join('dokter', 'dokter.id_dokter = jadwal.id_dokter');
      $this->db->join('poli', 'poli.id_poli = jadwal.id_poli');
      return $this->db->get();
   }

   function simpan_jadwal($data)
   {
      $this->db->insert('jadwal', $data);
   }

   function data_pasien()
   {
      $this->db->select('*')->from('pasien');
      return $this->db->get();
   }

   function data_kunjungan()
   {
      $this->db->select('*');
      $this->db->from('rekam_medis');
      $this->db->join('pasien', 'pasien.id_pasien = rekam_medis.id_pasien');
      $this->db->join('poli', 'poli.id_poli = rekam_medis.id_poli');
      return $this->db->get();
   }

   function data_pengaduan()
   {
      $this->db->select('*')->from('pengaduan');
      return $this->db->get();
   }

   function data_dimensi()
   {
      $this->db->select('*')->from('dimensi');
      return $this->db->get();
   }

   function simpan_dimensi($data)
   {
      $this->db->insert('dimensi', $data);
   }

   function tabel_dimensi()
   {
      $this->db->select('*');
      $this->db->from('dimensi');
      return $this->db->get();
   }

   function data_kuis()
   {
      $this->db->select('*');
      $this->db->from('kuisioner');
      $this->db->join('dimensi', 'dimensi.id_dimensi = kuisioner.id_dimensi');
      return $this->db->get();
   }

   function simpan_kuis($data)
   {
      $this->db->insert('kuisioner', $data);
   }

   function hasil_survey()
   {
      $this->db->select('*');
      $this->db->from('kuisioner');
      $this->db->join('jawab_kuis', 'jawab_kuis.id_kuis = kuisioner.id_kuis');
      $this->db->group_by('kuisioner.id_kuis');
      return $this->db->get();
   }

   function total_responden()
   {
      $this->db->select('id_pasien');
      $this->db->from('jawab_kuis');
      $this->db->group_by('id_pasien');
      return $this->db->get();
   }

   function simpan_pengaduan($data)
   {
      $this->db->insert('pengaduan', $data);
   }

   function daftar()
   {	
   		$this->db->select('COUNT(id_ins) as count')->from('instansi');
   		$query = $this->db->get();
   		if($query->num_rows() > 0)
   		{
   			$row = $query->row();
   			return $row->count;
   		}
   		return 0;
   }
}