<?php
class User_Model extends CI_Model
{
   function main()
   {
      parent::__construct();        
   }

   function simpan_daftar($array_data) {
        $this->db->insert('login', $array_data);
   }

   function simpan_profil($array_data)
   {
      $this->db->insert('pasien', $array_data);
   }

   function cek_login($username){
         $query = $this
            ->db
            ->where('username', $username)
            ->get('login');
         if($query->num_rows() == 1){
            return $query->row_array();
         }else{
            return FALSE;
         }
   }

   function cek_data($id_login)
   {
      $this->db->select('*')->from('pasien')->where('id_login', $id_login)->limit('1');
      return $this->db->get();
   }

   function cek_pasien($id_login)
   {
      $this->db->select('*')->from('pasien')->where('id_login', $id_login);
      return $this->db->get();
   }   

   function cek_rekam($no_rm)
   {
      $this->db->select('*')->from('rekam_medis')->where('no_rm', $no_rm);
      return $this->db->get();
   }

   function cek_daftar($tgl_daftar)
   {
      $this->db->select('*')->from('rekam_medis')->where('tgl_daftar', $tgl_daftar);
      return $this->db->get();
   }
   

   function data_kunjungan($id_pasien)
   {
      //$this->db->select('*')->from('rekam_medis')->where('id_pasien', $id_pasien);
      $this->db->select('*');
      $this->db->from('rekam_medis');
      $this->db->join('pasien', 'pasien.id_pasien = rekam_medis.id_pasien');
      $this->db->join('poli', 'poli.id_poli = rekam_medis.id_poli');
      $this->db->where('rekam_medis.id_pasien', $id_pasien);
      return $this->db->get();
   }

   function cek_survey($id_pasien)
   {
      $this->db->select('*')->from('pasien')->where('id_pasien', $id_pasien);
      return $this->db->get();
   }

   function soal()
   {
      $this->db->select('*')->from('kuisioner');
      return $this->db->get();
   }

   function simpan_survey($data)
   {
      $this->db->insert('jawab_kuis', $data);
   }

    function ubah_password($data, $id_ins)
   {
   		$this->db->where('id_ins', $id_ins);
   		$this->db->update('instansi', $data);
   }

    function update_profil($data, $id_login)
   {
   		$this->db->where('id_login', $id_login);
   		$this->db->update('pasien', $data);
   }

   function simpan_rekam_medis($data)
   {
      $this->db->insert('rekam_medis', $data);
   }

   function upload_surat($data)
   {
   		$this->db->insert('surat', $data);
   }

   function data_poli()
   {
      $this->db->select('*')->from('poli');
      return $this->db->get();
   }

}